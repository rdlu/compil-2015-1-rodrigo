/*
	Rodrigo Dlugokenski
*/


#ifndef TREE_H
#define TREE_H

#define MAX_FILHOS 4 

#include "cc_ast.h"
#include "cc_dict.h"
#include "utstring.h"

typedef struct comp_tree_t {
    int nodeType;
    int tipoIKS;
    int size;
    UT_string *code;
    UT_string *label_t;
    UT_string *label_f;
    UT_string *label;
    DictItem *dict_item; //ponteiro para tablela de simbolos
    struct comp_tree_t *filho[MAX_FILHOS]; //numero abritario de filhos
} *ASTNode;

ASTNode ASTNewNode(int nodeType, DictItem *dict_item, ASTNode filho_0, ASTNode filho_1, ASTNode filho_2, ASTNode filho_3);


void ASTPreorder(ASTNode a_node, void (*funcao)(ASTNode));
void ASTPostorder(ASTNode a_node, void (*funcao)(ASTNode));
void ASTPrintRecursive(ASTNode a_node);
void ASTPrint(ASTNode a_node);
void ASTGraphviz(ASTNode a_node);
void ASTDestroy(ASTNode root_node);

#endif
