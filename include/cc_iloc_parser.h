#ifndef __ILOCPARSER_H
#define __ILOCPARSER_H
#include <stdio.h>
#include "utarray.h"
#include "uthash.h"
#include "utstring.h"

//1 origem, 1 destino
#define OP_load           6385446277
#define OP_store          210728224082
//1 origem, 1 destino
#define OP_loadI          210719727214
//2 origem, 1 destino
#define OP_loadAI         6953750997871
#define OP_loadAO         6953750997877
//1 origem, 2 destino
#define OP_storeAI        229483036027516
#define OP_storeAO        229483036027522

//1 destino
#define OP_jump           6385381345
#define OP_jumpI          210717584458

//2 origens, 1 destino
#define OP_add            193486030
#define OP_addI           6385039063
#define OP_sub            193506191
#define OP_subI           6385704376
#define OP_rsubI           210727008202

#define OP_mul            193499667
#define OP_mulI           6385489084
#define OP_div            193489480
#define OP_divI           6385152913
#define OP_rdivI           210726456739

#define OP_and            193486360
#define OP_andI           6385049953
#define OP_or            5863686
#define OP_orI           193501711
#define OP_xor            193511454
#define OP_xorI           6385878055

#define OP_lshift           6953755999823
#define OP_lshiftI          229473947994232
#define OP_rshift           6953990812181
#define OP_rshiftI           229481696802046

//0 orig/dest
#define OP_nop            193500562
#define OP_halt           6385287662

//1 origem, 1 destino
#define OP_i2i            193493097
#define OP_i2c            193493091
#define OP_c2c            193486557

//1 origem, 2 destino
#define OP_cbr            193488156

//2 origem, 1 destino
#define OP_cmp_LT            6953396941476
#define OP_cmp_LE            6953396941461
#define OP_cmp_EQ            6953396941242
#define OP_cmp_GT            6953396941311
#define OP_cmp_GE            6953396941296
#define OP_cmp_NE            6953396941527



typedef struct iloc_s {
    UT_hash_handle hh;         /* makes this structure hashable */
    UT_string *label;
    UT_string *op;
    unsigned long op_num;
    int is_leader;
    UT_string *src1;
    UT_string *src2;
    int src_num;
    UT_string *dst1;
    UT_string *dst2;
    int dst_num;
    UT_string *line;
    UT_string *consumed_line;
    int jmp_from;
    UT_string *jmp_f;
    UT_array *jmp_fr;
} *ILOC_Struct;

ILOC_Struct labeled_lines;
UT_array *iloc_lines;
ILOC_Struct new_iloc_s();
ILOC_Struct parse_iloc_line(char *line);
ILOC_Struct parse_iloc_file(FILE *file_ptr);

typedef struct iloc_basic_block {
    int number;
    int start;
    int end;
    int visitado; //para dfs
    UT_array *destinations;
    UT_array *sources;
    UT_array *dominados;
    UT_array *dominadores;
    UT_hash_handle hh_start;        /* handle for hash table of LIDERES */
    UT_hash_handle hh_end;        /* handle for hash table of TERMINADORES */
} *ILOC_BBlock;

UT_array *iloc_basic_blocks;
UT_array *iloc_path;
ILOC_BBlock iloc_basic_blocks_by_start;
ILOC_BBlock iloc_basic_blocks_by_end;
#endif
