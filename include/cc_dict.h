/*
	Rodrigo Dlugokenski
*/

#ifndef HASH_H
#define HASH_H
#include "cc_semantic.h"
#include "uthash.h"
#include "utarray.h"
#include "utstring.h"

union Data
{
   int int_value;
   float float_value;
   char *string_value;
};

typedef struct _comp_dict_item_t {
    char *text;
	int linenum;
    int used_by_ast;
    int scope_level;
    UT_string *mem_pos;
	int token_type; //TK_* setado pelo scanner
    int identifier_type; //IKS_* tipo 100% certo setado SOMENTE pela DECLARACAO do parser
    //usar ASTNode->iksType para inferencia de tipos herdados da arvore
    int isA; //IS A VARIABLE,VECTOR,FUNCTION, setado pelo parser
    int argsn;
    int size;
    int is_static;
    int is_const;
    int is_param;
    UT_array *dimension_size; //usada em arrays (TK_IDENTIFIER)
    intlist* params;
	union Data data;
    UT_hash_handle hh;         /* makes this structure hashable */
} DictItem;

DictItem* hash_new();
DictItem* hash_add(DictItem *hashtable, char *text, int token_type);
DictItem* hash_find(DictItem *hashtable, char *text);
DictItem* hash_remove(DictItem *hashtable, char *text);
void hash_print(DictItem *hashtable);
void hash_destroy(DictItem *hashtable);

#endif
