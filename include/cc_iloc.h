#ifndef ILOC_H
#define ILOC_H

void ILOCGen(ASTNode node);
ASTNode ILOCGen_aux_aritm(ASTNode node, int reg, int curto, ASTNode pai, int nro_teste);
void ILOCCalculateDeslocamentoVetor(ASTNode var, ASTNode index, ASTNode child, int reg);
void ILOCCalculateDeslocamentoVetorChilds(ASTNode child, int reg, ASTNode var, int dim);
ASTNode ILOCGen_aux_attrib_direita(ASTNode node,int reg);
#endif