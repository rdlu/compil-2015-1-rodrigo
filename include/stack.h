#ifndef __STACK_H
#define __STACK_H

#include "cc_dict.h"

typedef struct _myStack{
	DictItem** tables;
	int size; 
} Stack;

Stack* sInit();
void sPush(Stack *stack, DictItem* table);
Stack* sPop(Stack *stack);
DictItem* sTop(Stack *stack);
DictItem* sTopmostN(Stack *stack, int n);
void sFree(Stack* stack);

#endif
