#ifndef __SCOPES_H
#define __SCOPES_H

#include "cc_dict.h"
#include "stack.h"
#include "cc_semantic.h"
#include "cc_tree.h"

void initScope(Stack* s); //inicializacao
void newScope(); //entrando em novo escopo
void endScope(); //saindo do escopo
DictItem* sFindAllButCurrent(char *text);
DictItem* sFindAllButCurrentDeclared(char *text);
int sFind(char* nome); //encontrar um identificador (na pilha)
DictItem* sFindCurrent(char* text);

int sFindHere(char* nome); //encontrar um identificador (no escopo atual)
void sAdd(char* nome, int identifier_type, int isA, intlist *params); //adicionar um identificador (no escopo atual)
void destroyAllScopes();
void finishScope(Stack* stack);
void isVector(char* nome);
void isFunction(char* nome);
void checkF(char* fun, intlist* args);
intlist* getArgs(ASTNode node);

#endif
