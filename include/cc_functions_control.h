#ifndef __FUNCTIONS_H
#define __FUNCTIONS_H
#include <stdio.h>
#include "cc_tree.h"
#include "cc_dict.h"
#include "utarray.h"
#include "uthash.h"

UT_array *callers_stack;
UT_array *temporary_param_fifo;
UT_array *temporary_local_var_fifo;

typedef struct function_info {
    char *name;
    char label[100];
    ASTNode ast_node;
    UT_array *param_names;
    UT_array *param_sizes;
    UT_array *param_pos;
    UT_array *local_vars_names;
    UT_array *local_vars_sizes;
    UT_array *local_vars_pos;
    int rarp_size;
    int state_pos;
    int static_pos;
    int din_pos;
    int return_value_pos;
    int return_addr_pos;
    int params_pos;
    int vars_pos;
    UT_hash_handle hh;
} *FunctionInfo;

FunctionInfo declare_function(ASTNode function_node);
FunctionInfo find_function(char *name);

int find_param_pos(FunctionInfo f_info, DictItem *item);
int find_var_pos(FunctionInfo f_info, DictItem *item);

void push_function_param(struct _comp_dict_item_t *item);
void push_function_local_var(struct _comp_dict_item_t *item);


#endif
