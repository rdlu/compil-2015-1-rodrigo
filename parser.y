/*
	Grupo Heroes of The Storm
	Arthur Ribeiro
	Rodrigo Dlugokenski
	Rafael de Jesus Martins
*/
%{
#include <stdio.h>
#include <main.h>
#include <string.h>
#include "cc_dict.h"
#include "cc_tree.h"
#include "cc_semantic.h"
#include "scopes.h"
#include "cc_functions_control.h"
    
ASTNode ast_root;
Stack* escopos;
int pos;
UT_array *dimension_size;
FunctionInfo current_func = NULL;
%}

%union {
	struct _comp_dict_item_t *dict_item; //HashNode from cc_dict.h/c
    struct comp_tree_t *ast; //ASTNode from cc_tree.h/c
	int itype;
	struct ilist *ilist;
    int val;
}

%type <ast> expressao
%type <ast> identificador
%type <ast> literal
%type <ast> chamada_de_funcao
%type <ast> lista_argumentos
%type <ast> lista_expressoes
%type <ast> fluxo_de_controle
%type <ast> retorno
%type <ast> comando_svazio
%type <ast> comando
%type <ast> entrada
%type <ast> else_opc
%type <ast> atribuicao
%type <ast> saida
%type <ast> bloco_de_cmds
%type <ast> lista_de_cmds
%type <ast> declaracao_local
%type <ast> funcao
%type <ast> corpo
%type <ast> cabecalho
%type <ast> varglobal_ou_funcao
%type <ast> lista_varglobais_e_funcoes
%type <ast> identificador_nova_fun
%type <ast> multidimen
%type <itype> tipo
%type <dict_item> identificador_novo
%type <ilist> parametros_opc
%type <ilist> lista_parametros
%type <itype> parametro
%type <dict_item> decl_multidimen
%type <val> static_opc
%type <val> const_opc

/* Declaração dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_STATIC
%token TK_PR_CONST
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token <dict_item> TK_LIT_INT
%token <dict_item> TK_LIT_FLOAT
%token <dict_item> TK_LIT_FALSE
%token <dict_item> TK_LIT_TRUE
%token <dict_item> TK_LIT_CHAR
%token <dict_item> TK_LIT_STRING
%token <dict_item> TK_IDENTIFICADOR
%token TOKEN_ERRO

%left '+' '-'
%left '*' '/' '%'
%left TK_OC_NE TK_OC_EQ TK_OC_GE TK_OC_LE '<' '>'
%left TK_OC_OR
%left TK_OC_AND
%right '!'
%left '(' ')'
%left '='
%right TK_PR_THEN TK_PR_ELSE
%nonassoc ':' '[' ']' ';' ','
arctic
%start p

%%
/* Regras (e ações) da gramática */


//Programa eh um conjunto de variaveis globais e funcoes (ambas opcionais e intercalaveis)

p: { initScope(escopos); newScope(); } programa { endScope(); finishScope(escopos); } ;


programa: lista_varglobais_e_funcoes { 
    ast_root = ASTNewNode(AST_PROGRAMA,0,$1,0,0,0);
    //ASTGraphviz(ast_root);
    ILOCGen(ast_root);
} 
	|   { 
        ast_root = ASTNewNode(AST_PROGRAMA,0,0,0,0,0);
        //ASTGraphviz(ast_root);
        ILOCGen(ast_root);
    } 
	;

lista_varglobais_e_funcoes: 
	varglobal_ou_funcao { $$ = $1; }
    //se for FUNCAO, adota o prox da lista, caso contrario ignora/continua em frente
	| varglobal_ou_funcao lista_varglobais_e_funcoes { if($1!=0) {$1->filho[MAX_FILHOS-1] = $2; $$=$1; } else {$$ = $2;} }
	;

//Varivel termina com `;`, funcao nao
varglobal_ou_funcao:
	declaracao_global ';'  { $$ = 0; }
	| funcao               { $$ = $1; }
	;

//regra para declaração de variáveis globais
declaracao_global: 
	static_opc tipo identificador_novo { sAdd($3->text, $2, VARIAVEL, NULL); }
	| declaracao_vetor
	;
//regra para declaração de variáveis locais
declaracao_local: 
	static_opc const_opc tipo identificador_novo inicializacao_local_opc { $$ = 0; sAdd($4->text, $3, VARIAVEL, NULL); $4->is_static = ($1)?1:0; $4->is_const = ($2)?1:0; push_function_local_var($4); }
	;

//regra para inicializaçao local opicional
inicializacao_local_opc:
	TK_OC_LE literal // TK_OC_LE = "<=", usado na inicializacao
	| TK_OC_LE identificador
	| 
	;

//regra para declaração de vetor
declaracao_vetor: 
    static_opc tipo identificador_novo '['TK_LIT_INT decl_multidimen ']'  {  utarray_push_back($5->dimension_size,&$5->data.int_value); if($6) utarray_concat($5->dimension_size,$6->dimension_size); utarray_concat($3->dimension_size,$5->dimension_size); sAdd($3->text, $2, VETOR, (intlist*) (long) ($5->size*(($6)?$6->size:1))); $3->is_static = ($1)?1:0; } //ATENCAO!! NAO EH UM PONTEIRO DE ILIST, E SIM O VALOR DO TOKEN DE INTEIRO, PASSADO COMO UM ENDEREÇO!!!
	;

decl_multidimen: 
    ',' TK_LIT_INT decl_multidimen  { 
            if($3) { utarray_push_back($2->dimension_size,&$2->data.int_value); utarray_concat($2->dimension_size,$3->dimension_size); $2->size = $2->size * $3->size;  $$ = $2;  } 
            else { utarray_push_back($2->dimension_size,&$2->data.int_value); $$ = $2; }
    }
    | { $$ = 0; }
;

//regra para declaraçao de função
funcao: cabecalho corpo	   { $$ = ASTNewNode(AST_FUNCAO,$<ast>1->dict_item,$2,0,0,0); if ((!$2) || ($2->tipoIKS == 0)) { $$->tipoIKS = $1->tipoIKS; } else { $$->tipoIKS = returnChecker($1->tipoIKS,$2->tipoIKS); }; _ASTDelete($1); current_func = declare_function($$); }

	| cabecalho corpo ';' {yyerror("Definicao de funcao nao termina com ponto-e-virgula."); return SINTATICA_ERRO;}
	;

//regra para o cabeçalho da função
cabecalho:
	static_opc tipo identificador_nova_fun '(' parametros_opc ')' { $$ = $3; $$->tipoIKS = $2; sAdd($$->dict_item->text, $2, FUNCAO, $5); $3->dict_item->is_static = ($1)?1:0; }
	;

//parametros opcionais do cabeçalho de uma função (vazio ou lista de parametros)
parametros_opc:
	{ pos = 0; } lista_parametros { $$ = $2; }
	| { $$ = NULL; }
	;
//lista de parametros, propriamente (parametros separados por virgula)
lista_parametros:
	parametro { $$ = malloc(sizeof(intlist)) ; $$->val = $1; $$->next = NULL; }
	| parametro',' lista_parametros { $$ = malloc(sizeof(intlist)); $$->val = $1; $$->next = $3; }
	
	| parametro';' lista_parametros {yyerror("Lista de parametros com ';' e o correto eh ',' "); return SINTATICA_ERRO;}
	;

parametro:
	const_opc tipo identificador_novo { sAdd($3->text, $2, VARIAVEL, NULL); $3->is_param = 1; push_function_param($3); $$ = $2; }
	;

//corpo de uma funçao
corpo: bloco_de_cmds { $$ = $1; }
	;

bloco_de_cmds:
	   { newScope(); }'{' lista_de_cmds '}' { $$ = $3; endScope(); }

	//| '{' lista_de_cmds '}' ';' {yyerror("Erro: Bloco de comandos terminado por ';'!");}
	;

lista_de_cmds:
	comando                     { $$ = $1; }
    //se nao for DECL_LOCAL, adota o prox da lista, caso contrario ignora/continua em frente
	| comando ';' lista_de_cmds { if($1!=0) {$1->filho[MAX_FILHOS-1] = $3; $$=$1; } else {$$ = $3;} }
	;

//comandos simples (secao 2.3)
comando:
	comando_svazio { $$ = $1; }
	|              { $$ = 0; }
	;

comando_svazio:
	declaracao_local         { $$ = $1; }
	| atribuicao             { $$ = $1; }
	| entrada                { $$ = $1; }
	| saida                  { $$ = $1; }
	| retorno                { $$ = $1; }
	| fluxo_de_controle      { $$ = $1; }
	| chamada_de_funcao      { $$ = $1; }
	| bloco_de_cmds          { $$ = $1; }
	;

atribuicao:
	identificador '=' expressao      { $$ = ASTNewNode(AST_ATRIBUICAO,0,$1,$3,0,0); $$->tipoIKS = cohersionChecker($1->tipoIKS,$3->tipoIKS); checkUsage(0,isA($1->dict_item->text)); } 
	| identificador'[' expressao multidimen ']' '=' expressao { ASTNode v = ASTNewNode(AST_VETOR_INDEXADO,0,$1,$3,0,0); $$ = ASTNewNode(AST_ATRIBUICAO,0,v,$7,0,0); v->filho[3] = $4; isVector($1->dict_item->text); isNum($3->tipoIKS); }
	| literal '=' expressao {yyerror("Literal usado no lado esquerdo de uma atribuicao! "); return SINTATICA_ERRO;}
	| literal '[' expressao ']' '=' expressao {yyerror("Literal usado no lado esquerdo de uma atribuicao! "); return SINTATICA_ERRO;}
	;

entrada:
	TK_PR_INPUT expressao '=''>' expressao  { $$ = ASTNewNode(AST_INPUT,0,$2,$5,0,0); isId($2->nodeType); isId($5->nodeType); }
	;

saida:
	TK_PR_OUTPUT lista_expressoes    { $$ = ASTNewNode(AST_OUTPUT,0,$2,0,0,0); isOut($2->nodeType, $2->tipoIKS); }
	;

retorno:
	TK_PR_RETURN expressao           { $$ = ASTNewNode(AST_RETURN,0,$2,0,0,0); $$->tipoIKS = $2->tipoIKS; }
	;

fluxo_de_controle:
	TK_PR_IF '(' expressao ')' TK_PR_THEN comando_svazio else_opc   { $$ = ASTNewNode(AST_IF_ELSE,0,$3,$6,$7,0); }
	| TK_PR_WHILE '(' expressao ')' TK_PR_DO comando_svazio         { $$ = ASTNewNode(AST_WHILE_DO,0,$3,$6,0,0); }
	| TK_PR_DO comando_svazio TK_PR_WHILE '(' expressao ')'         { $$ = ASTNewNode(AST_DO_WHILE,0,$2,$5,0,0); }
	;

else_opc:
	 %prec TK_PR_THEN { $$=0; }//forcar o shift, quando tiver um else
	| TK_PR_ELSE comando_svazio { $$=$2; }
	;

chamada_de_funcao:
	identificador '(' lista_argumentos ')'  { $$ = ASTNewNode(AST_CHAMADA_DE_FUNCAO,0,$1,$<ast>3,0,0); isFunction($1->dict_item->text); $$->tipoIKS = $1->tipoIKS; checkF($1->dict_item->text, getArgs($3)); }
	;

lista_expressoes:
	expressao                            { $$ = $1; }
	| expressao ',' lista_expressoes     { $1->filho[MAX_FILHOS-1] = $3; $$ = $1; }
//	| expressao ';' lista_expressoes {yyerror("Lista de parametros com ';' e o correto eh ',' "); return TOKEN_ERRO;}
	;

//expressoes aritmeticas e logicas (sec 2.5)
expressao:
	literal                                 { $$ = $1; }
	| identificador                        { $$ = $1; }
	| identificador '[' expressao multidimen ']'  { $$ = ASTNewNode(AST_VETOR_INDEXADO,0,$1,$3,0,0); $$->filho[3] = $4; $$->tipoIKS = $1->tipoIKS; isVector($1->dict_item->text); isNum($3->tipoIKS); }
	| chamada_de_funcao                   { $$ = $1; }
	| '(' expressao ')'                   { $$ = $2; }
	| expressao '+' expressao             { $$ = ASTNewNode(AST_ARIM_SOMA,0,$1,$3,0,0);
						$$->tipoIKS = cohersionChecker($1->tipoIKS,$3->tipoIKS);}
	| expressao '-' expressao             { $$ = ASTNewNode(AST_ARIM_SUBTRACAO,0,$1,$3,0,0);
						$$->tipoIKS = cohersionChecker($1->tipoIKS,$3->tipoIKS);}
	| expressao '*' expressao             { $$ = ASTNewNode(AST_ARIM_MULTIPLICACAO,0,$1,$3,0,0);
						$$->tipoIKS = cohersionChecker($1->tipoIKS,$3->tipoIKS);}
	| expressao '/' expressao             { $$ = ASTNewNode(AST_ARIM_DIVISAO,0,$1,$3,0,0); 
						$$->tipoIKS = cohersionChecker($1->tipoIKS,$3->tipoIKS);}
	| expressao '%' expressao             { $$ = ASTNewNode(AST_ARIM_DIVISAO,0,$1,$3,0,0); 
						$$->tipoIKS = cohersionChecker($1->tipoIKS,$3->tipoIKS);}
	| expressao '<' expressao             { $$ = ASTNewNode(AST_LOGICO_COMP_L,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao '>' expressao             { $$ = ASTNewNode(AST_LOGICO_COMP_G,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao TK_OC_LE expressao        { $$ = ASTNewNode(AST_LOGICO_COMP_LE,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao TK_OC_GE expressao        { $$ = ASTNewNode(AST_LOGICO_COMP_GE,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao TK_OC_EQ expressao        { $$ = ASTNewNode(AST_LOGICO_COMP_IGUAL,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao TK_OC_NE expressao        { $$ = ASTNewNode(AST_LOGICO_COMP_DIF,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao TK_OC_AND expressao       { $$ = ASTNewNode(AST_LOGICO_E,0,$1,$3,0,0);
						$$->tipoIKS = IKS_BOOL;}
	| expressao TK_OC_OR expressao        { $$ = ASTNewNode(AST_LOGICO_OU,0,$1,$3,0,0); 
						$$->tipoIKS = IKS_BOOL;}
//	| '"' TK_LIT_STRING '"'              
	| '-' expressao                       { $$ = ASTNewNode(AST_ARIM_INVERSAO,0,$2,0,0,0); 
						$$->tipoIKS = $2->tipoIKS;}
        | '!' expressao                       { $$ = ASTNewNode(AST_LOGICO_COMP_NEGACAO,0,$2,0,0,0); 
						$$->tipoIKS = $2->tipoIKS;}
	;

lista_argumentos:
	lista_expressoes   { $$=$1; }
	|                  { $$ = 0; }
	;

multidimen: ',' expressao multidimen  { if($2) { $2->filho[3] = $3; $$ = $2; } else { $$ = 0; } }
| { $$ = 0; };

//regra para o identificador
identificador: 
	TK_IDENTIFICADOR          { $$ = ASTNewNode(AST_IDENTIFICADOR,$1,0,0,0,0); $$->tipoIKS = checkDeclaration($$); }
				  //  $$->tipoIKS = $$->dict_item->comp_dict_t....;} VERIFICAR!!!    ;

identificador_novo:
	TK_IDENTIFICADOR { $$ = $1; }
	;
	
identificador_nova_fun:
    TK_IDENTIFICADOR {  $$ = ASTNewNode(AST_IDENTIFICADOR,$1,0,0,0,0); }
    ;

//regra para static opcional
const_opc: 
TK_PR_CONST {$$=$<val>1;} | {$$=0;};

//regra para static opcional
static_opc: 
TK_PR_STATIC {$$=$<val>1;}| {$$=0;};

//regra para os tipos suportados
//TODO: AST_LITERAL, será?
tipo:	TK_PR_INT   	{$$ = IKS_INT; }//size = 4;}                 
        | TK_PR_FLOAT  	{$$ = IKS_FLOAT; }//size = 8;}             
	| TK_PR_CHAR    {$$ = IKS_CHAR; }//size = 1;}            
        | TK_PR_BOOL    {$$ = IKS_BOOL; }//size = 1;}          
        | TK_PR_STRING  {$$ = IKS_STRING; }//size = 1;}          
	;

//regra para os literais suportados
literal: TK_LIT_INT                  { $$ = ASTNewNode(AST_LITERAL,$1,0,0,0,0); $$->tipoIKS = IKS_INT; }
	| TK_LIT_FLOAT                   { $$ = ASTNewNode(AST_LITERAL,$1,0,0,0,0); $$->tipoIKS = IKS_FLOAT;}
	| TK_LIT_FALSE                   { $$ = ASTNewNode(AST_LITERAL,$1,0,0,0,0); $$->tipoIKS = IKS_BOOL;}
	| TK_LIT_TRUE                    { $$ = ASTNewNode(AST_LITERAL,$1,0,0,0,0); $$->tipoIKS = IKS_BOOL;}
	| TK_LIT_CHAR                    { $$ = ASTNewNode(AST_LITERAL,$1,0,0,0,0); $$->tipoIKS = IKS_CHAR;}
	| TK_LIT_STRING                  { $$ = ASTNewNode(AST_LITERAL,$1,0,0,0,0); $$->tipoIKS = IKS_STRING;}

	;


%%
