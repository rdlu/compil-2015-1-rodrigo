#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "scopes.h"
#include "cc_semantic.h"
#include "cc_tree.h"

#define debug_print(fmt, ...) \
            do { if (DEBUG) fprintf(stderr, fmt, __VA_ARGS__); } while (0)

#define DEBUG 0

//extern Stack escopos;
Stack* escopos;
extern DictItem *current_table;
int mem_pos = 0;

void initScope(Stack* s){
	s = sInit();
	escopos = s;
}

void newScope(){
	DictItem* newS;
    mem_pos = 0;
	newS = hash_new();
	sPush(escopos, newS);
    current_table = newS;
    debug_print("NEW SCOPE :: [%d] :: [%p]\n",escopos->size,current_table);
}

void endScope(){
    debug_print("END SCOPE :: [%d] :: [%p]\n",escopos->size,current_table);
	escopos = sPop(escopos);
    current_table = sTop(escopos);
}

void destroyAllScopes() {
    while(current_table != NULL) {
        endScope();
    }
}

DictItem* sFindAllButCurrent(char *text) {
    int i;
    DictItem* id = NULL;
    for(i = escopos->size-1; i>0; i--) {
        debug_print("SEARCH STACK %s :: [%d] :: [%p]\n",text,i,escopos->tables[i-1]);
        id = hash_find(escopos->tables[i-1], text);
        if(id) {
            debug_print("FOUND! SEARCH STACK %s :: [%d] :: [%p / %p]\n",id->text,i,escopos->tables[i-1],id);
            break;    
        }
    }
    
    return id;
}

DictItem* sFindAllButCurrentDeclared(char *text) {
    int i;
    DictItem* id = NULL;
    for(i = escopos->size-1; i>0; i--) {
        debug_print("SEARCH STACK %s :: [%d] :: [%p]\n",text,i,escopos->tables[i-1]);
        id = hash_find(escopos->tables[i-1], text);
        if(id && (id->identifier_type != 0)) {
            debug_print("FOUND DECLARED! SEARCH STACK %s :: [%d] :: [%p / %p]\n",id->text,i,escopos->tables[i-1],id);
            break;    
        }
    }
    
    return id;
}

DictItem* sFindCurrent(char* text){
    debug_print("SEARCH STACK CURRENT %s\n",text);
    DictItem* id = hash_find(sTop(escopos), text);
	if (id) debug_print("FOUND! SEARCH STACK CURRENT %s :: [%d] :: [%p / %p]\n",id->text,escopos->size-1,sTop(escopos),id);
	return id;
}

int sFind(char* nome){
    DictItem* nodo = sFindAllButCurrentDeclared(nome);
	if (nodo != NULL)
		return nodo->identifier_type;    
    return 0;
}

int isA(char* nome){
    DictItem* nodo = sFindCurrent(nome);
	if (!nodo || (nodo && nodo->identifier_type == 0)) nodo = sFindAllButCurrentDeclared(nome);
	if (nodo)
		return nodo->isA;
    return 0;
}



int sFindHere(char* nome){
    DictItem* id = sFindCurrent(nome);
	if (id)
		return id->identifier_type;
	return 0;
}



void sAdd(char* nome, int identifier_type, int isA, intlist* params){
    //fprintf(stderr,"sadd called :: [%s]\n",nome);
	DictItem* id = sFindCurrent(nome);
    debug_print("SADD %s :: %s [%p] found\n",nome,id->text,id);
    
    //o item jah deveria estar definido pelo scanner
    if ( !id ) {
        fprintf(stderr,">>>>>>>>Funcao sAdd: O item [%s] ja deveria estar definido pelo scanner\n",nome);
        exit(666);
    }
		
    
    //tentando definir tipo de um item ja definido neste escopo
    if ( id->identifier_type != 0)
		exit(IKS_ERROR_DECLARED);
    
    //fprintf(stderr,"VECT %s [%d]\n",id->text,id->size);

	id->isA = isA;
    id->scope_level = escopos->size - 1;
	id->params = params;
    id->identifier_type = identifier_type;
    if(!id->is_param) utstring_printf(id->mem_pos,"%d",mem_pos);
    else utstring_printf(id->mem_pos,"%d",-1);
	int vetor = 1;
	if (isA == VETOR)
	    vetor = ((long) params) + id->size;
	switch (identifier_type)
	{
	    case IKS_BOOL:
	    case IKS_CHAR:
	        id->size = CHAR_SIZE * vetor;
	        break;
	    case IKS_INT:
	        id->size = INT_SIZE * vetor;
	        break;
	    case IKS_FLOAT:
	        id->size = FLOAT_SIZE * vetor;
	        break;
	    case IKS_STRING:
	        id->size = CHAR_SIZE * strlen(nome) * vetor;
	        break;
	}
    if(!id->is_param) mem_pos = mem_pos + id->size;
    
   /* DictItem* aux = id->next_dimension;
    while(aux) {
        utarray_push_back(id->dimension_size,&aux->data.int_value);
        debug_print("DIMENSION ADDED: %p [%d] {%p}\n",aux,aux->data.int_value,aux->next_dimension);
        aux = aux->next_dimension;
    }
    
    debug_print("DIMENSIONS: %d\n",utarray_len(id->dimension_size));*/
}

void finishScope(Stack* stack){
    sFree(stack);
}

void isVector(char* nome){
    int is;
    is = isA(nome);
    if(is == VARIAVEL)
        iksError(IKS_ERROR_VARIABLE);
    if(is == FUNCAO)
        iksError(IKS_ERROR_FUNCTION);       
}

void isFunction(char* nome){
        int is;
    is = isA(nome);
    if(is == VARIAVEL)
        iksError(IKS_ERROR_VARIABLE);
    if(is == VETOR)
        iksError(IKS_ERROR_VECTOR);   
}

void checkF(char* fun, intlist* args){
	int i;
	DictItem* nodo = sFindAllButCurrent(fun);
	if (nodo != NULL)
		checkArgs(nodo->params,args);
	else iksError(IKS_ERROR_UNDECLARED);
}

intlist* getArgs(ASTNode node){
    intlist* args = NULL;
    if (node != NULL)
    {
        args = malloc (sizeof(intlist));
        args->val = node->tipoIKS;
        args->next = getArgs(node->filho[MAX_FILHOS-1]);
    }
    return args;
}
