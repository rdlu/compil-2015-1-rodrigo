#include "cc_iloc_parser.h"
#include <string.h>
#include <ctype.h>
#include <errno.h>


UT_icd iloc_s_icd = {sizeof(struct iloc_s), NULL, NULL, NULL};
UT_icd iloc_basic_blocks_icd = {sizeof(struct iloc_basic_block), NULL, NULL, NULL};
int contains = 1;

ILOC_Struct new_iloc_s() {
    ILOC_Struct st = calloc(1,sizeof(struct iloc_s));
    
    utstring_new(st->label);
    utstring_new(st->op);
    utstring_new(st->src1);
    utstring_new(st->src2);
    utstring_new(st->dst1);
    utstring_new(st->dst2);
    utstring_new(st->line);
    utstring_new(st->jmp_f);
    utstring_new(st->consumed_line);
    
    utarray_new(st->jmp_fr,&ut_int_icd);
    
    return st;
}

//http://www.cse.yorku.ca/~oz/hash.html
unsigned long
hash_chars(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

void inplace_trim(char* s)
{
    int start, end = strlen(s);
    for (start = 0; s[start] && isspace(s[start]); ++start) {}
    if (s[start]) {
        while (end > 0 && isspace(s[end-1]))
            --end;
    }
    memmove(s, &s[start], end - start);
    s[end - start] = '\0';
}

char* copy_trim(const char* s)
{
    int start, end;
    for (start = 0; s[start] && isspace(s[start]); ++start) {}
    if (s[start] == '\0') return strdup("");
    for (end = strlen(s); end > 0 && isspace(s[end-1]); --end) {}
    return strndup(s + start, end - start);
}

int parse_iloc_label(UT_string *line, UT_string *dst, UT_string *consumed_line) {
    int pos;
    char *aux = utstring_body(line);
    pos = utstring_find(line,0,":",1);
    if(pos > 0) {
        char *f_label;
        f_label = strndup(aux,pos);
        //fprintf(stderr,"Label encontrado em %d [%s]\n",pos,f_label);
        utstring_bincpy(dst, f_label, pos);
        //fprintf(stderr,"Parsed label:: %s\n",utstring_body(dst));
        inplace_trim(&aux[pos+1]);
        utstring_bincpy(consumed_line, &aux[pos+1], strlen(&aux[pos+1]));
        //fprintf(stderr,"Parsed label [consumed_line]:: %s\n",utstring_body(consumed_line));
        return 1;
    } else {
        utstring_printf(consumed_line,"%s",aux);
        return 0;
    }
}

int parse_iloc_op(UT_string *consumed_line, UT_string *dst) {
    char *r_line = utstring_body(consumed_line);
    int pos = utstring_find(consumed_line,0," ",1);
    if(pos<0) pos = strlen(r_line); 
    utstring_bincpy(dst, r_line, pos);
    //fprintf(stderr,"Parsed operation:: %s\n",utstring_body(dst));
    inplace_trim(&r_line[pos]);
    //fprintf(stderr,"Remaining::.%s\n",&r_line[pos]);
    utstring_clear(consumed_line);
    //fprintf(stderr,"Parsed operation [consumed_line]:: %s\n",utstring_body(consumed_line));
    //utstring_bincpy(consumed_line, &r_line[pos], strlen(&r_line[pos]));
    utstring_printf(consumed_line,"%s",&r_line[pos]);
    //fprintf(stderr,"Parsed operation [consumed_line]:: %s\n",utstring_body(consumed_line));
}

void set_iloc_op(ILOC_Struct struc) {
    struc->op_num = hash_chars(utstring_body(struc->op));
    
    switch(struc->op_num) {
        case OP_nop:
        case OP_halt:
            struc->src_num = 0;
            struc->dst_num = 0;
            break;
        case OP_cbr:
            struc->src_num = 1;
            struc->dst_num = 2;
            break;
        case OP_jump:
        case OP_jumpI:
            struc->src_num = 0;
            struc->dst_num = 1;
            break;
        case OP_load:
        case OP_store:
        case OP_loadI:
        case OP_i2i:
        case OP_i2c:
        case OP_c2c:
            struc->src_num = 1;
            struc->dst_num = 1;
            break;
        case OP_storeAI:
        case OP_storeAO:
            struc->src_num = 1;
            struc->dst_num = 2;
            break;
        default:
            struc->src_num = 2;
            struc->dst_num = 1;
    }
}

void parse_iloc_dst(ILOC_Struct struc, int dst_num) {
    char *needle = ">";
    if(struc->dst_num == 2 && dst_num == 1) needle = ",";
    int pos;
    char *aux = utstring_body(struc->consumed_line);
    //fprintf(stderr,"AUX111 line parse_iloc_dst:: %s\n",aux);
    pos = utstring_findR(struc->consumed_line,-1,needle,1);
    inplace_trim(&aux[pos+1]);
    aux = copy_trim(aux);
    //if(dst_num == 1) utstring_printf(struc->dst1, "%s", &aux[pos+1]);
    if(dst_num == 1 && struc->dst_num == 2) utstring_printf(struc->dst2, "%s", &aux[pos+1]); 
    else utstring_printf(struc->dst1, "%s", &aux[pos+1]);
    //fprintf(stderr,"Parsed dst:: [%s], [%s] [%d]\n",utstring_body(struc->dst1),utstring_body(struc->dst2),dst_num);
    utstring_clear(struc->consumed_line);
    //fprintf(stderr,"AUX line parse_iloc_dst:: %s\n",aux);
    //remove so a ,
    if(struc->dst_num == 2 && dst_num == 1) utstring_bincpy(struc->consumed_line, aux, pos);
    //remove =>
    else utstring_bincpy(struc->consumed_line, aux, pos-1);
    //fprintf(stderr,"Consumed line after parse_iloc_dst:: %s\n",utstring_body(struc->consumed_line));
}

void parse_iloc_src(ILOC_Struct struc, int src_num) {
    int pos;
    char *aux = utstring_body(struc->consumed_line);
    if(struc->src_num == 2) {
        pos = utstring_find(struc->consumed_line,0,",",1);
        char *aux2;
        if(src_num == 1) {
            aux2 = strndup(aux,pos);
            inplace_trim(aux2);
            utstring_printf(struc->src1, "%s", aux2);
        } else {
            aux2 = &aux[pos+1];
            inplace_trim(aux2);
            utstring_printf(struc->src2, "%s", aux2);
        }
    } else {
        inplace_trim(aux);
         utstring_printf(struc->src1, "%s", aux);
    }
    
    //fprintf(stderr,"Parsed src:: [%s], [%s] [%d]\n",utstring_body(struc->src1),utstring_body(struc->src2),src_num);
}

ILOC_Struct parse_iloc_line(char *line) {
    ILOC_Struct parsed = new_iloc_s();
    inplace_trim(line);
    utstring_printf(parsed->line,"%s",line);
    //fprintf(stderr,"Received line:: %s\n",line);
    //fprintf(stderr,"Parsed line:: %s\n",utstring_body(parsed->line));
    if(parse_iloc_label(parsed->line, parsed->label,parsed->consumed_line)) {
        //ADICIONA NA HASH PARA POSTERIOR LOCALIZACAO
        HASH_ADD_KEYPTR(hh, labeled_lines, utstring_body(parsed->label), utstring_len(parsed->label), parsed);
    }
    //fprintf(stderr,"Consumed line after label:: %s\n",utstring_body(parsed->consumed_line));
    parse_iloc_op(parsed->consumed_line,parsed->op);
    set_iloc_op(parsed);
    //fprintf(stderr,"Parsed operation:: %s [%ld] S: %d D: %d\n",utstring_body(parsed->op), parsed->op_num, parsed->src_num, parsed->dst_num);
    //fprintf(stderr,"Consumed line after op:: %s\n",utstring_body(parsed->consumed_line));
    int i;
    for(i = 1; i <= parsed->dst_num; i++) {
        parse_iloc_dst(parsed,i);
    }
    //fprintf(stderr,"Consumed line after parse_iloc_dst:: %s\n",utstring_body(parsed->consumed_line));
    
    for(i = 1; i <= parsed->src_num; i++) {
        parse_iloc_src(parsed,i);
    }
    return parsed;
}

void iloc_set_leaders(UT_array *lines) {
    ILOC_Struct current_line, next_line;
    //a primeira eh lider
    current_line = (ILOC_Struct) utarray_eltptr(lines,0);
    current_line->is_leader = 1;
    utstring_printf(current_line->jmp_f,"%d;",0);
    
    
    
    int i;
    for(i=0;i<utarray_len(lines);i++) {
        current_line = (ILOC_Struct) utarray_eltptr(lines,i);
        switch(current_line->op_num) {
            case OP_cbr:
                //quem CBR aponta como dst2
                next_line = NULL;
                HASH_FIND_STR(labeled_lines, utstring_body(current_line->dst2), next_line);
                if(next_line) {
                    next_line->is_leader = 1;
                    next_line->jmp_from = i;
                    utstring_printf(next_line->jmp_f,"%d;",i);
                    utarray_push_back(next_line->jmp_fr,&i);
                }
                //fprintf(stderr,"CBR!!!!! %s, %s [%d] [%p]\n", utstring_body(current_line->dst1),utstring_body(next_line->label),next_line->is_leader,&next_line->is_leader);
            case OP_jumpI:
                //quem jmpI,CBR aponta como dst1
                next_line = NULL;
                HASH_FIND_STR(labeled_lines, utstring_body(current_line->dst1), next_line);
                if(next_line) {
                    next_line->is_leader = 1;
                    next_line->jmp_from = i;
                    utstring_printf(next_line->jmp_f,"%d;",i);
                    utarray_push_back(next_line->jmp_fr,&i);
                }
                //fprintf(stderr,"JMPI!!!!! %s, %s [%d] [%p]\n", utstring_body(current_line->dst1),utstring_body(next_line->label),next_line->is_leader,&next_line->is_leader);
            case OP_jump:
                //apos jmp,jmpI,cbr
                next_line = NULL;
                next_line = (ILOC_Struct) utarray_eltptr(lines,i+1);
                next_line->is_leader = 1;
                next_line->jmp_from = i;
                utstring_printf(next_line->jmp_f,"%d;",i);
                //utarray_push_back(next_line->jmp_fr,&i);
                break;
            default:
                //nothing
                break;
        }
    }
    current_line->is_leader = 1;
    for(i=0;i<utarray_len(lines);i++) {
        current_line = (ILOC_Struct) utarray_eltptr(lines,i);
        //fprintf(stderr,"L%d: [%s] [%s] -> {%s}\n",i,utstring_body(current_line->label),utstring_body(current_line->op),utstring_body(current_line->jmp_f));
    }
}

ILOC_BBlock iloc_new_bblock() {
    ILOC_BBlock st = calloc(1,sizeof(struct iloc_basic_block));
    utarray_new(st->destinations, &ut_int_icd);
    utarray_new(st->sources, &ut_int_icd);
    utarray_new(st->dominados, &ut_int_icd);
    utarray_new(st->dominadores, &ut_int_icd);
    return st;
}

void iloc_create_basic_blocks(UT_array *iloc_lines) {
    iloc_basic_blocks_by_start = NULL;
    iloc_basic_blocks_by_end = NULL;
    utarray_new(iloc_basic_blocks,&iloc_basic_blocks_icd);
    ILOC_BBlock current_block = iloc_new_bblock();
    //cria os blocos
    int blk_num = 0;
    int i;
    ILOC_Struct current_line = (ILOC_Struct) utarray_eltptr(iloc_lines,0);
    current_block->number = blk_num++;
    current_block->start = 0;
    
    for(i=1;i<utarray_len(iloc_lines);i++) {
        current_line = (ILOC_Struct) utarray_eltptr(iloc_lines,i);
        if(utstring_len(current_line->jmp_f)?1:0) {
            //eh lider, finaliza bloco com cara anterior
            current_block->end = i-1;
            utarray_push_back(iloc_basic_blocks,current_block);
            current_block = iloc_new_bblock();
            current_block->number = blk_num++;
            current_block->start = i;
        }        
    }
    current_block->end = i-1;
    utarray_push_back(iloc_basic_blocks,current_block);
    current_block = NULL;
    
    for(i=0;i<utarray_len(iloc_basic_blocks);i++) {
        current_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,i);
        HASH_ADD(hh_start, iloc_basic_blocks_by_start, start, sizeof(int), current_block);
        HASH_ADD(hh_end, iloc_basic_blocks_by_end, end, sizeof(int), current_block);
        //fprintf(stderr,"Bloco %d: [%d-%d]\n",current_block->number,current_block->start,current_block->end);
    }
}

ILOC_BBlock iloc_create_flow_graph(UT_array *iloc_lines, UT_array *iloc_basic_blocks) {
    ILOC_BBlock current_block,target_block;
    ILOC_Struct start_line,end_line;
    int i,j;
    for(i=0;i<utarray_len(iloc_basic_blocks);i++) {
        current_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,i);
        start_line = (ILOC_Struct) utarray_eltptr(iloc_lines,current_block->start);
        end_line = (ILOC_Struct) utarray_eltptr(iloc_lines,current_block->end);
        int target;
        for(j=0;j<utarray_len(start_line->jmp_fr);j++) {
            target = *((int*)utarray_eltptr(start_line->jmp_fr,j));
            HASH_FIND(hh_end, iloc_basic_blocks_by_end, &target, sizeof(int), target_block);
            utarray_push_back(target_block->destinations,&current_block->number);
            utarray_push_back(current_block->sources,&target_block->number);
        }
        
        int x;
        switch(end_line->op_num) {
            case OP_cbr:            
            case OP_jump:
            case OP_jumpI:
                break;
            default:
                x = current_block->number+1;
            if(x<utarray_len(iloc_basic_blocks)) {
                utarray_push_back(current_block->destinations,&x);
                target_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,x);
                utarray_push_back(target_block->sources,&current_block->number);
            }
        }
    }
    
    //hack para sequencia de ativacao simples
    for(i=0;i<utarray_len(iloc_basic_blocks);i++) {
        current_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,i);
        if(utarray_len(current_block->destinations)==0){
           
            int j,ultimo_dominado,aux;
            for(j=current_block->number+1;j<utarray_len(iloc_basic_blocks);j++) {
                ultimo_dominado = 0;
                contains = 1;
                aux = iloc_check_dominador(current_block->number,j,0); 
                //fprintf(stderr,"HACK %d %d %d\n",current_block->number,j,aux);
                if(aux) {
                    ultimo_dominado = j;
                }         
            }
            //esta dentro dos limites
            if(ultimo_dominado > 0 && ultimo_dominado != current_block->number && ultimo_dominado< utarray_len(iloc_basic_blocks)) {
                 utarray_push_back(current_block->destinations,&ultimo_dominado);
            }
        }
    }
    
    for(i=0;i<utarray_len(iloc_basic_blocks);i++) {
        current_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,i);
        fprintf(stderr,"Bloco %d: [%d-%d] -> ",current_block->number,current_block->start,current_block->end);   
        for(j=0;j<utarray_len(current_block->destinations);j++) {
            fprintf(stderr,"[bloco %d]",*((int*)utarray_eltptr(current_block->destinations,j)));
        }
/*        fprintf(stderr,"   <--- ");
        for(j=0;j<utarray_len(current_block->sources);j++) {
            fprintf(stderr,"[bloco %d]",*((int*)utarray_eltptr(current_block->sources,j)));
        }*/
        fprintf(stderr,"\n");
    }
        
}

int iloc_check_dominador(int dominador, int dominado, int origem) {
    ILOC_BBlock block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks, origem);
    int i;
    block->visitado = 1;
    utarray_push_back(iloc_path,&block->number);
    //fprintf(stderr,"Visitando %d\n",origem);
    if(origem == dominado) {
        int k;
        contains = 0;
        for(i=0;i<utarray_len(iloc_path);i++) {
            //tenho o caminho aqui
            k = *((int*) utarray_eltptr(iloc_path,i));
            if(dominador == k) contains = 1;
            //fprintf(stderr," {%d}",k);            
        }
       // fprintf(stderr,"\n CON %d %d\n",contains,dominador);
    } else {
        //percorro as adj
        int j;
        for(i=0;i<utarray_len(block->destinations);i++) {
            j = *((int*) utarray_eltptr(block->destinations,i));
            ILOC_BBlock aux = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks, j);
            if(contains && !aux->visitado) contains = iloc_check_dominador(dominador,dominado,j);
        }
    }
    block->visitado = 0;
    utarray_pop_back(iloc_path);
    return contains;
}

ILOC_Struct parse_iloc_file(FILE *file_ptr) {
    ILOC_Struct parsed_struct;
    labeled_lines = NULL;
    utarray_new(iloc_lines,&iloc_s_icd);
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    utarray_new(iloc_path,&ut_int_icd);
    
    if (file_ptr == NULL)
       exit(errno);
    
    while ((read = getline(&line, &len, file_ptr)) != -1) {
        //line[strlen(line)] = '\0';
        //printf("Retrieved line of length %zu :\n", read);
        parsed_struct = parse_iloc_line(line);
        //todo: guardar na array
        utarray_push_back(iloc_lines, parsed_struct);
    }
    fprintf(stderr,"\n[ILOC PARSER] Numero de linhas encontradas: %d\n",utarray_len(iloc_lines));
    
    iloc_set_leaders(iloc_lines);
    fprintf(stderr,"\n:: BLOCOS ::\n");
    iloc_create_basic_blocks(iloc_lines);
    iloc_create_flow_graph(iloc_lines,iloc_basic_blocks);
    
    

    int dominador,dominado;
    ILOC_BBlock block;
    fprintf(stderr,"\n:: DOMINADORES ::\n");
    for(dominador=0;dominador<utarray_len(iloc_basic_blocks);dominador++) {
        fprintf(stderr,"%d domina ",dominador);
        for(dominado=0;dominado<utarray_len(iloc_basic_blocks);dominado++) {
            utarray_erase(iloc_path,0,utarray_len(iloc_path));
            contains = 1;
            if(iloc_check_dominador(dominador,dominado,0)) {
                block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks, dominador);
                utarray_push_back(block->dominados,&dominado);
                block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks, dominado);
                utarray_push_back(block->dominadores,&dominador);
                fprintf(stderr,"%d, ",dominado);
                //fprintf(stderr,"[DOM: %d dom %d] \n",dominador,dominado);
            }
        }
        fprintf(stderr,"\n");
    }
    
    //deteccao de loops
    fprintf(stderr,"\n:: LOOPS ::\n");
    int i,j,dest;
    ILOC_BBlock current_block, dest_block;
    for(i=0;i<utarray_len(iloc_basic_blocks);i++) {
        current_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,i);
        for(j=0;j<utarray_len(current_block->destinations);j++) {
            dest = *((int*)utarray_eltptr(current_block->destinations,j));
            dest_block = (ILOC_BBlock) utarray_eltptr(iloc_basic_blocks,dest);
            if(dest < current_block->number) {
                fprintf(stderr,"Backtick entre %d-%d (maybe loop, verificando dominadores)\n",current_block->number,dest);
                contains = 1;
                if(iloc_check_dominador(dest,current_block->number,0)) {
                    fprintf(stderr,"Loop detectado entre %d-%d\n",current_block->number,dest);
                } else {
                    fprintf(stderr," %d-%d Nao eh Loop\n",current_block->number,dest);
                }
            }
        }
    }
    
    //Movimentacao de invariantes
    //TODO: Movimentacao de invariantes dos loops
    
    //Imprimindo a estrutura (que deve estar otimizada (ou nao))
    for(i=0;i<utarray_len(iloc_lines);i++) {
        ILOC_Struct current_line = (ILOC_Struct) utarray_eltptr(iloc_lines,i); 
        if(utstring_len(current_line->label)>0) fprintf(stdout,"%s: ",utstring_body(current_line->label));
        else fprintf(stdout,"\t");
        fprintf(stdout,"%s ",utstring_body(current_line->op));
        if(current_line->src_num == 1) fprintf(stdout,"%s",utstring_body(current_line->src1));
        if(current_line->src_num == 2) fprintf(stdout,"%s,%s",utstring_body(current_line->src1),utstring_body(current_line->src2));
        if(current_line->dst_num > 0)fprintf(stdout," => ");
        if(current_line->dst_num == 1) fprintf(stdout,"%s",utstring_body(current_line->dst1));
        if(current_line->dst_num == 2) fprintf(stdout,"%s,%s",utstring_body(current_line->dst1),utstring_body(current_line->dst2));
        fprintf(stdout,"\n");
    }
    
    free(line);
    //fclose(stream);
}