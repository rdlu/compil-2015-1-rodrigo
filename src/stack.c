#include <stdlib.h>
#include <stdio.h>
#include "stack.h"


Stack* sInit(){
	Stack* s;
	if (s = malloc (sizeof(Stack)))
{   
	s->size = 0;
	s->tables = calloc (sizeof(DictItem*), 20);
	
	if (s->tables != NULL)
	    return s;
}
    return NULL;
}
void sPush(Stack *stack, DictItem* table){
	stack->tables[stack->size++] = table; //nova tabela no stack; size++
    //fprintf(stderr,"PUSH STACK :: [%d] :: [%p]\n",stack->size,sTop(stack));
}
Stack* sPop(Stack *stack){
	hash_destroy(sTop(stack));
	stack->size--;
    //fprintf(stderr,"POP STACK :: [%d] :: [%p]\n",stack->size,sTop(stack));
	return stack;
}

DictItem* sTop(Stack *stack){
    if(stack->size == 0) return NULL;
	return stack->tables[(stack->size)-1];
}

//DEPRECATED
DictItem* sTopmostN(Stack *stack, int n){
    fprintf(stderr,"Using sTopMostN, deprecated N %d",stack->size -(n+2));
	if ((stack->size)-n <= 0)
		return NULL;
	else
		return stack->tables[stack->size -(n+2)];
}

void sFree(Stack* stack){
    free (stack->tables);
    free (stack);
}
