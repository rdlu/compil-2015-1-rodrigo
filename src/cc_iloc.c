#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cc_tree.h"
#include "cc_ast.h"
#include "cc_semantic.h"
#include "utstring.h"
#include "utarray.h"
#include "cc_iloc.h"
#include "cc_functions_control.h"

#define CURTO_ON 0
#define DEBUG 0

int unique_int = 0;
int nro_teste_linear = 0;
int line = 0;
int f_return = 0;
extern FunctionInfo current_func;
extern FILE *iloc_out;

ASTNode ILOCGen_aux_attrib_esquerda(ASTNode node) {
    ASTNode aux_node;
    switch(node->nodeType) {
        case AST_IDENTIFICADOR:
            if(node->dict_item->scope_level == 0) {
                if(DEBUG) fprintf(iloc_out,"\t//@%s em %s\n",node->dict_item->text,utstring_body(node->dict_item->mem_pos));
                fprintf(iloc_out,"\tstoreAI r0 => rbss,%s\n",utstring_body(node->dict_item->mem_pos));
                line++;
            } else {
                fprintf(iloc_out,"\tstoreAI r0 => fp,%d\n",find_var_pos(current_func,node->dict_item));
                line++;
            }
            break;
        case AST_VETOR_INDEXADO:
            //ILOCGen_aux_attrib_direita(node->filho[0],3);
            ILOCCalculateDeslocamentoVetor(node->filho[0],node->filho[1],node->filho[3],3);
            if(node->filho[0]->dict_item->scope_level == 0) {
                if(DEBUG) fprintf(iloc_out,"\t//@%s em %s\n",node->filho[0]->dict_item->text,utstring_body(node->dict_item->mem_pos));
                //fprintf(iloc_out,"\tadd rbss,r3 => r3\n");
                fprintf(iloc_out,"\tstoreAI r0 => r3,%s\n",utstring_body(node->filho[0]->dict_item->mem_pos)); line++;
            } else {
                //fprintf(iloc_out,"\tadd fp,r3 => r3\n");
                fprintf(iloc_out,"\tstoreAI r0 => r3,%d\n",find_var_pos(current_func,node->filho[0]->dict_item)); line++;
            }
            break;
        default:
            fprintf(stderr,"WARN: ILOCGen_aux_attrib_esquerda com tipo nao suportado %d\n",node->nodeType);  
            break;
    }    
    return node;
}

ASTNode ILOCGen_aux_attrib_direita(ASTNode node,int reg) {
    ASTNode aux_node;
    char* deslocamento;
    char *segment;
    switch(node->nodeType) {
        case AST_IDENTIFICADOR:
            if(node->dict_item->scope_level == 0 && (!node->dict_item->is_param)) {
                if(DEBUG) fprintf(iloc_out,"\t//@%s em %s\n",node->dict_item->text,utstring_body(node->dict_item->mem_pos));
                fprintf(iloc_out,"\tloadAI rbss,%s => r%d\n",utstring_body(node->dict_item->mem_pos),reg);
                line++;
            } else if(node->dict_item->is_param) {
                fprintf(iloc_out,"\tloadAI fp,%d => r%d\n",find_param_pos(current_func,node->dict_item),reg);
            } else {
                fprintf(iloc_out,"\tloadAI fp,%d => r%d\n",find_var_pos(current_func,node->dict_item),reg);
                line++;
            }
            break;
        case AST_VETOR_INDEXADO:
            ILOCGen_aux_attrib_direita(node->filho[0],3);
            ILOCCalculateDeslocamentoVetor(node->filho[0],node->filho[1],node->filho[3],reg);
            break;
        default:
            fprintf(stderr,"WARN: ILOCGen_aux_attrib_direita com tipo nao suportado %d\n",node->nodeType);  
            break;
    }
    return node;
}

void ILOCCalculateDeslocamentoVetor(ASTNode var, ASTNode index, ASTNode childs, int reg) {
    int unit_size = 1;
    int desloc_index = 0;
    UT_string* retorno;
    utstring_new(retorno);
    switch(var->tipoIKS) {
        case IKS_INT:
        case IKS_BOOL:
            unit_size = INT_SIZE;
            break;
        case IKS_FLOAT:
            unit_size = FLOAT_SIZE;
            break;
        case IKS_CHAR:            
        case IKS_STRING:        
            unit_size = CHAR_SIZE;
            break;
    }
    
    
    switch(index->nodeType) {
        case AST_LITERAL:
            desloc_index = index->dict_item->data.int_value;
            utstring_printf(retorno,"%d",desloc_index*4);
            
             if(childs) { 
                int desloc_dim = 1;
                int *p;
                int i;
                for(i = 1; i < utarray_len(var->dict_item->dimension_size);i++) {
                    p = (int*) utarray_eltptr(var->dict_item->dimension_size,i);
                    desloc_dim = desloc_dim * (*p);
                }
                 fprintf(iloc_out,"\tloadI %d => r%d\n",desloc_dim*desloc_index*4,reg);
                 line++;
                //fprintf(iloc_out,"\tmulI r%d,%d => r40\n",40,desloc_dim);
                if(DEBUG) fprintf(iloc_out,"\t//Deslocamento de vetor em +(%d*4*%d)\n",desloc_index,desloc_dim);
                
             } else {
                 fprintf(iloc_out,"\tloadI %d => r%d\n",desloc_index*4,reg);
                 line++;
                if(DEBUG) fprintf(iloc_out,"\t//Deslocamento de vetor em +%d*4\n",desloc_index);
             }

            break;
        case AST_ARIM_SOMA:
        case AST_ARIM_SUBTRACAO:
        case AST_ARIM_MULTIPLICACAO:
        case AST_ARIM_DIVISAO:
        case AST_ARIM_INVERSAO:
        case AST_LOGICO_E:
        case AST_LOGICO_OU:
        case AST_IDENTIFICADOR:
            fprintf(iloc_out,"\tload rbss => r%d\n",3); line++;
            ILOCGen_aux_aritm(index,reg+1,0,NULL,0);
            utstring_printf(retorno,"r%d",reg+1);
        
            if(childs) {
                int desloc_dim = 1;
                int *p;
                int i;
                for(i = 1; i < utarray_len(var->dict_item->dimension_size);i++) {
                    p = (int*) utarray_eltptr(var->dict_item->dimension_size,i);
                    desloc_dim = desloc_dim * (*p);
                }
                 //fprintf(iloc_out,"\tloadI %d => r40\n",desloc_dim*desloc_index*4);
                fprintf(iloc_out,"\tmulI r%d,%d => r%d\n",reg+1,desloc_dim*4,reg+1); line++;
                if(DEBUG) fprintf(iloc_out,"\t//Deslocamento de vetor em +(%s*4*%d)\n",utstring_body(retorno),desloc_dim);
                
             } else {
                fprintf(iloc_out,"\tmulI r%d,%d => r%d\n",reg+1,4,reg+1); line++;
                if(DEBUG) fprintf(iloc_out,"\t//Deslocamento de vetor em +%s*4\n",utstring_body(retorno));
             }
            fprintf(iloc_out,"\tadd r%d,r%d => r%d\n",reg,reg+1,reg); line++;
            break;
        default:
            fprintf(stderr,"WARN: ILOCCalculateDeslocamentoVetor nodeType nao suportado ainda :: %d\n",index->nodeType);
    }   
    
    if(childs) ILOCCalculateDeslocamentoVetorChilds(childs,reg,var,1);
    
}

void ILOCCalculateDeslocamentoVetorChilds(ASTNode child, int reg, ASTNode var, int dim) {
    int desloc_index = 0;
    int desloc_dim = 1;
    UT_string* retorno;
    utstring_new(retorno);
    switch(child->nodeType) {
        case AST_LITERAL:
            desloc_index = child->dict_item->data.int_value;
            utstring_printf(retorno,"%d",desloc_index);
            
            if(child->filho[3]) { 
                int *p;
                int i;
                for(i = dim; i < utarray_len(var->dict_item->dimension_size);i++) {
                    p = (int*) utarray_eltptr(var->dict_item->dimension_size,i);
                    desloc_dim = desloc_dim * (*p);
                }
                //fprintf(iloc_out,"\tmulI r%d,%d => r%d\n",reg,desloc_dim,reg);
                fprintf(iloc_out,"\taddI r%d,%d => r%d\n",reg,desloc_index*4*desloc_dim,reg); line++;
            } else {
                fprintf(iloc_out,"\taddI r%d,%d => r%d\n",reg,desloc_index*4,reg); line++;
            }        
            if(DEBUG) fprintf(iloc_out,"\t//Deslocamento de vetor em +(%d*4*%d)\n",desloc_index,desloc_dim);
            break;
        case AST_ARIM_SOMA:
        case AST_ARIM_SUBTRACAO:
        case AST_ARIM_MULTIPLICACAO:
        case AST_ARIM_DIVISAO:
        case AST_ARIM_INVERSAO:
        case AST_LOGICO_E:
        case AST_LOGICO_OU:
        case AST_IDENTIFICADOR:
            ILOCGen_aux_aritm(child,reg+1,0,NULL,0);
            utstring_printf(retorno,"r%d",reg);
            if(child->filho[3]) { 
                int *p;
                int i;
                for(i = dim; i < utarray_len(var->dict_item->dimension_size);i++) {
                    p = (int*) utarray_eltptr(var->dict_item->dimension_size,i);
                    desloc_dim = desloc_dim * (*p);
                }
                fprintf(iloc_out,"\tmulI r%d,%d => r%d\n",reg+1,desloc_dim*4,reg+1); line++;
            } else {
                fprintf(iloc_out,"\tmulI r%d,%d => r%d\n",reg+1,4,reg+1); line++;
            }
            fprintf(iloc_out,"\tadd r%d,r%d => r%d\n",reg,reg+1,reg); line++;
            if(DEBUG) fprintf(iloc_out,"\t//Deslocamento de vetor em +(%s*4*%d)\n",child->dict_item->text,desloc_dim);
            break;
        default:
            fprintf(stderr,"WARN: ILOCCalculateDeslocamentoVetorChilds nodeType nao suportado ainda :: %d\n",child->nodeType);
    }
    
    
    if(child->filho[3]) ILOCCalculateDeslocamentoVetorChilds(child->filho[3],reg,var,dim+1);
}

ASTNode ILOCGen_aux_literal(ASTNode node, int reg) {
    if(node)
    switch(node->tipoIKS) {
        case IKS_INT:
            fprintf(iloc_out,"\tloadI %d => r%d\n",node->dict_item->data.int_value,reg); line++;
            break;
        case IKS_FLOAT:
            fprintf(iloc_out,"\tloadI %f => r%d\n",node->dict_item->data.float_value,reg); line++;
            break;
        case IKS_BOOL:
            fprintf(iloc_out,"\tloadI %d => r%d\n",node->dict_item->data.int_value,reg); line++;
            break;
        default:
            fprintf(stderr,"WARN: ILOCGen_aux_literal IKS nao suportado ainda :: %d\n",node->tipoIKS);
            break;
    
    }
    
}

void ILOCGenCBR(int reg, UT_string *label_true, UT_string *label_false) {
    fprintf(iloc_out,"\tcbr r%d -> %s, %s\n",reg,utstring_body(label_true),utstring_body(label_false));
    line++;
}

ASTNode ILOCGen_aux_aritm(ASTNode node, int reg, int curto, ASTNode pai, int nro_teste) {
    if(node->filho[0])
    switch(node->filho[0]->nodeType) {
        case AST_LITERAL:
        case AST_IDENTIFICADOR:
            ILOCGen_aux_aritm(node->filho[0],reg+1,curto,pai,nro_teste+1);
            break;
        case AST_VETOR_INDEXADO:
            break;
        case AST_ARIM_SOMA:
        case AST_ARIM_SUBTRACAO:
        case AST_ARIM_MULTIPLICACAO:
        case AST_ARIM_DIVISAO:
        case AST_ARIM_INVERSAO:
            ILOCGen_aux_aritm(node->filho[0],reg+100,0,NULL,nro_teste+1);
            fprintf(iloc_out,"\ti2i r%d => r%d\n",reg+100,reg+1);
            line++;
            break;
        default:
            ILOCGen_aux_aritm(node->filho[0],reg+100,curto,pai,nro_teste+1);
            fprintf(iloc_out,"\ti2i r%d => r%d\n",reg+100,reg+1);
            line++;
            break;
    }
    
    if(node->filho[1])
    switch(node->filho[1]->nodeType) {
        case AST_LITERAL:
        case AST_IDENTIFICADOR:
            ILOCGen_aux_aritm(node->filho[1],reg+2,curto,pai,nro_teste+1);
            break;
        case AST_VETOR_INDEXADO:
            break;
        case AST_ARIM_SOMA:
        case AST_ARIM_SUBTRACAO:
        case AST_ARIM_MULTIPLICACAO:
        case AST_ARIM_DIVISAO:
        case AST_ARIM_INVERSAO:
            ILOCGen_aux_aritm(node->filho[1],reg+200,0,NULL,nro_teste+1);
            fprintf(iloc_out,"\ti2i r%d => r%d\n",reg+200,reg+2);
            line++;
            break;
        default:
            ILOCGen_aux_aritm(node->filho[1],reg+200,curto,pai,nro_teste+1);
            fprintf(iloc_out,"\ti2i r%d => r%d\n",reg+200,reg+2);
            line++;
            break;
    }
    
    if(nro_teste == 0) nro_teste_linear = 0;
    if(pai) fprintf(iloc_out,"%s-%d%d: nop\n",utstring_body(pai->label),nro_teste,nro_teste_linear++);
    line++;
    switch(node->nodeType) {
        case(AST_ARIM_SOMA):
            fprintf(iloc_out,"\tadd r%d,r%d => r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_ARIM_SUBTRACAO):
            fprintf(iloc_out,"\tsub r%d,r%d => r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_ARIM_MULTIPLICACAO):
            fprintf(iloc_out,"\tmul r%d,r%d => r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_ARIM_DIVISAO):
            fprintf(iloc_out,"\tdiv r%d,r%d => r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_ARIM_INVERSAO):
            fprintf(iloc_out,"\tloadI 0 => r%d\n\tsub r%d, r%d => r%d;\n",reg+2,reg+2,reg+1,reg); line++;
            break;
        case(AST_LOGICO_E):
            fprintf(iloc_out,"\tand r%d,r%d => r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_OU):
            fprintf(iloc_out,"\tor r%d,r%d => r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_DIF):
            fprintf(iloc_out,"\tcmp_NE r%d,r%d -> r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_IGUAL):
            fprintf(iloc_out,"\tcmp_EQ r%d,r%d -> r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_LE):
            fprintf(iloc_out,"\tcmp_LE r%d,r%d -> r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_GE):
            fprintf(iloc_out,"\tcmp_GE r%d,r%d -> r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_L):
            fprintf(iloc_out,"\tcmp_LT r%d,r%d -> r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_G):
            fprintf(iloc_out,"\tcmp_GT r%d,r%d -> r%d\n",reg+1,reg+2,reg); line++;
            break;
        case(AST_LOGICO_COMP_NEGACAO):
            fprintf(iloc_out,"\tloadI 1 => r%d\n\tcmp_NE r%d, r%d -> r%d\n",reg+2,reg+2,reg+1,reg); line++;
            break;
        case(AST_LITERAL):
            ILOCGen_aux_literal(node,reg);
            break;
        case(AST_IDENTIFICADOR):
            ILOCGen_aux_attrib_direita(node,reg);
            break;
        case AST_VETOR_INDEXADO:
            ILOCGen_aux_attrib_direita(node,reg);
            break;
        default:
            fprintf(stderr,"ILOCGen_aux_aritm :: %d\n",node->nodeType);
            break;
    }
    
}

void ILOCGenFunctionDeclare(ASTNode func_node) {
    FunctionInfo f_info = find_function(func_node->dict_item->text);
    current_func = f_info;
    sprintf(f_info->label,"f-%s-%d",func_node->dict_item->text,unique_int++);
    
    if(strcmp(func_node->dict_item->text,"main") == 0) {
        //Main
        fprintf(iloc_out,"f-main: addI sp, %d => sp\n",f_info->rarp_size); line++;
        //Faz o que tem que fazer
        ILOCGen(func_node->filho[0]);
        if(f_return) {
            fprintf(iloc_out,"\tstoreAI r0 => fp, %d\n",f_info->return_value_pos); line++;
            f_return = 0;
        }
        //para o programa, main acabada
        fprintf(iloc_out,"\thalt\n"); line++;
        //Gera as proximas funcoes
        ILOCGen(func_node->filho[3]);

    } else {
        //Atualiza FP e SP
        fprintf(iloc_out,"%s: i2i sp => fp\n",f_info->label); line++;
        fprintf(iloc_out,"\taddI sp, %d => sp\n",f_info->rarp_size); line++;
        //Faz o que tem que fazer
        ILOCGen(func_node->filho[0]);
        //Ajusta os retornos
        if(f_return) {
            fprintf(iloc_out,"\tstoreAI r0 => fp, %d\n",f_info->return_value_pos); line++;
            f_return = 0;
        }
        fprintf(iloc_out,"\tloadAI fp,%d => r1\n",f_info->return_addr_pos); line++;
        //Ajusta FP e SP
        fprintf(iloc_out,"\tloadAI fp, %d => sp\n",f_info->static_pos); line++;
        fprintf(iloc_out,"\tloadAI fp, %d => fp\n",f_info->din_pos); line++;
        fprintf(iloc_out,"\tjump => r1\n"); line++;
        //Gera as proximas funcoes
        ILOCGen(func_node->filho[3]);
        
    }
    
    
}

void ILOCGenFunctionParamCalc(FunctionInfo callee_info, ASTNode expression_node, int param_num) {
    ILOCGen_aux_aritm(expression_node, 0, 0, NULL, 0);
    fprintf(iloc_out,"\tstoreAI r0 => sp, %d\n",(*(int*)utarray_eltptr(callee_info->param_pos,param_num))); line++;
    if(expression_node->filho[3]) ILOCGenFunctionParamCalc(callee_info,expression_node->filho[3],param_num+1);
}

void ILOCGenFunctionCall(ASTNode callee_node) {
    FunctionInfo callee_info = find_function(callee_node->filho[0]->dict_item->text);
    //fprintf(stderr,"@@@Called %s @ %s\n",callee_info->name,callee_info->label);
    if(strcmp(callee_node->filho[0]->dict_item->text,"main") == 0) {
        //Main
        fprintf(stderr,"LIMITACAO CONHECIDA: Chamada recursiva de main nao suportada!\n");
    } else {
        //Calcula/guarda os parametros
        if(callee_node->filho[1]) ILOCGenFunctionParamCalc(callee_info, callee_node->filho[1], 0);
        //Guardando vinculo est/din
        //Vínculo Estático (ponteiro para o RA do pai estático)        
        //Guardando FP e SP
        fprintf(iloc_out,"\tstoreAI sp => sp, %d\n",callee_info->static_pos); line++;
        //Vínculo Dinâmico (ponteiro para o RA do pai dinâmico) (EH O FP)
        fprintf(iloc_out,"\tstoreAI fp => sp, %d\n",callee_info->din_pos); line++;
        //Guardando end de retorno
        fprintf(iloc_out,"\tstoreAI %d => sp, %d\n",line+3,callee_info->return_addr_pos); line++;
        fprintf(iloc_out,"\tjumpI => %s\n",callee_info->label); line++;
        //Pegando o valor retornado e disponibilizando em r0
        fprintf(iloc_out,"\tloadAI sp, %d => r0\n",callee_info->return_value_pos); line++;
    }
}


void ILOCGen(ASTNode node) {
    ASTNode aux_node;
    if(node)
    switch(node->nodeType) {
        case(AST_PROGRAMA):
            fprintf(iloc_out,"\tloadI 0 => fp\n\tloadI 0 => sp\n\tloadI 0 => rbss\n"); line++;
            fprintf(iloc_out,"\tjumpI => f-main\n"); line++;
            if(node->filho[0]) ILOCGen(node->filho[0]);
            
            break;
        case(AST_IDENTIFICADOR):
            if(node->dict_item->scope_level == 0) {
                if(DEBUG) fprintf(iloc_out,"\t//@%s em %s\n",node->dict_item->text,utstring_body(node->dict_item->mem_pos));
                fprintf(iloc_out,"\tloadAI rbss,%s => r0\n",utstring_body(node->dict_item->mem_pos));
                 line++;
            } else {
                fprintf(iloc_out,"\tloadAI fp,%d => r0\n",find_var_pos(current_func,node->dict_item));
                 line++;
            }
            break;
        case(AST_FUNCAO):
            ILOCGenFunctionDeclare(node);
            break;
        case(AST_BLOCO):
            ILOCGen(node->filho[0]);
        case(AST_ATRIBUICAO):
            //fprintf(iloc_out,"attrib-%d:\n",unique_int++);
            ILOCGen(node->filho[1]);
            aux_node = ILOCGen_aux_attrib_esquerda(node->filho[0]);
            
            if(node->filho[3]) ILOCGen(node->filho[3]);
            break;
        case(AST_LITERAL):
            ILOCGen_aux_literal(node,0);
            break;
        case(AST_IF_ELSE):
            unique_int++;
            utstring_printf(node->label_f,"i-%d-f",unique_int);
            utstring_printf(node->label_t,"i-%d-t",unique_int);
            utstring_printf(node->label,"i-%d",unique_int);
            fprintf(iloc_out,"i-%d: nop\n",unique_int); line++;
            ILOCGen_aux_aritm(node->filho[0],10,CURTO_ON,node,0);
            if(!CURTO_ON) ILOCGenCBR(10,node->label_t,node->label_f);
            fprintf(iloc_out,"%s: nop\n",utstring_body(node->label_t));
            ILOCGen(node->filho[1]);
            fprintf(iloc_out,"\tjumpI -> %s-e\n",utstring_body(node->label)); line++;
            fprintf(iloc_out,"%s: nop\n",utstring_body(node->label_f)); line++;
            if(node->filho[2]) ILOCGen(node->filho[2]);
            fprintf(iloc_out,"%s-e: nop\n",utstring_body(node->label)); line++;
            if(node->filho[3]) ILOCGen(node->filho[3]);
            break;
        case(AST_WHILE_DO):
            unique_int++;
            utstring_printf(node->label_f,"w-%d-f",unique_int);
            utstring_printf(node->label_t,"w-%d-t",unique_int);
            utstring_printf(node->label,"w-%d",unique_int);
            fprintf(iloc_out,"w-%d: nop\n",unique_int); line++;
            ILOCGen_aux_aritm(node->filho[0],10,CURTO_ON,node,0);
            if(!CURTO_ON) ILOCGenCBR(10,node->label_t,node->label_f);
            fprintf(iloc_out,"%s: nop\n",utstring_body(node->label_t)); line++;
            ILOCGen(node->filho[1]);
            fprintf(iloc_out,"\tjumpI -> %s\n",utstring_body(node->label)); line++;
            fprintf(iloc_out,"%s: nop\n",utstring_body(node->label_f)); line++;
            if(node->filho[3]) ILOCGen(node->filho[3]);
            break;
        case(AST_DO_WHILE):
            unique_int++;
            utstring_printf(node->label_f,"d-%d-f",unique_int);
            utstring_printf(node->label_t,"d-%d-t",unique_int);
            utstring_printf(node->label,"d-%d",unique_int);
            fprintf(iloc_out,"d-%d: nop\n",unique_int); line++;
            fprintf(iloc_out,"%s: nop\n",utstring_body(node->label_t)); line++;
            ILOCGen(node->filho[0]);
            ILOCGen_aux_aritm(node->filho[1],10,CURTO_ON,node,0);
            if(!CURTO_ON) ILOCGenCBR(10,node->label_t,node->label_f);
            fprintf(iloc_out,"%s: nop\n",utstring_body(node->label_f)); line++;
            if(node->filho[3]) ILOCGen(node->filho[3]);
            break;
        case(AST_VETOR_INDEXADO):
            ILOCGen_aux_aritm(node,20,0,NULL,0);
            fprintf(iloc_out,"\tload r20 => r0\n"); line++;
            break;
        case(AST_ARIM_SOMA):
        case(AST_ARIM_SUBTRACAO):
        case(AST_ARIM_MULTIPLICACAO):
        case(AST_ARIM_DIVISAO):
        case(AST_ARIM_INVERSAO):
        case(AST_LOGICO_E):
        case(AST_LOGICO_OU):
        case(AST_LOGICO_COMP_IGUAL):
        case(AST_LOGICO_COMP_LE):
        case(AST_LOGICO_COMP_GE):
        case(AST_LOGICO_COMP_L):
        case(AST_LOGICO_COMP_G):
        case(AST_LOGICO_COMP_NEGACAO):
            ILOCGen_aux_aritm(node,0,0,NULL,0);
            break;
        case(AST_RETURN):
            ILOCGen_aux_aritm(node->filho[0],0,0,NULL,0);
            f_return = 1;
            break;
        case(AST_CHAMADA_DE_FUNCAO):
            ILOCGenFunctionCall(node);
            break;
        default:
            fprintf(stderr,"AST_DEFAULT :: %d\n",node->nodeType);
            break;
    }

}

