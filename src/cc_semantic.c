/*
	Grupo Heroes of The Storm
	Arthur Ribeiro
	Rodrigo Dlugokenski
	Rafael de Jesus Martins
*/
#include <stdlib.h>
#include <stdio.h>
#include "cc_semantic.h"
#include "cc_ast.h"
#include "cc_tree.h"

void iksError(int code) {
    fprintf(stderr,"Erro de tipo IKS #%d\n",code);
    yyerror("IKS ERROR");
    main_finalize();
    exit(code);
}

int checkDeclaration(ASTNode a_node) {
    //O token estah em alguma hash table e jah foi declarado
    // O Bison passa antes nas declaracoes e jah tem define o tipo
    int decl_type = sFindHere(a_node->dict_item->text);
    //verifica se nao tem nas tabelas acima
    if(decl_type == 0) {
        decl_type = sFind(a_node->dict_item->text); 
    }
    //se continua 0, entao nao foi declarada
    if(decl_type == 0) iksError(IKS_ERROR_UNDECLARED);
    return decl_type;
}

int cohersionChecker(int destiny, int source){    
	switch(destiny){
		case IKS_INT:

			switch(source){
				case IKS_INT: return IKS_INT;
				case IKS_FLOAT: return IKS_FLOAT;
				case IKS_BOOL: return IKS_INT;
				case IKS_CHAR: iksError(IKS_ERROR_CHAR_TO_X);
				case IKS_STRING: iksError(IKS_ERROR_STRING_TO_X);
			}break;

		case IKS_FLOAT: 
			switch(source){
				case IKS_INT: return IKS_FLOAT;
				case IKS_FLOAT: return IKS_FLOAT;
				case IKS_BOOL: return IKS_FLOAT;
				case IKS_CHAR: iksError(IKS_ERROR_CHAR_TO_X);
				case IKS_STRING: iksError(IKS_ERROR_STRING_TO_X);
			}break;

		case IKS_BOOL:
 		
			switch(source){
				case IKS_INT: return IKS_INT;
				case IKS_FLOAT: return IKS_FLOAT;
				case IKS_BOOL: return IKS_BOOL;
				case IKS_CHAR: iksError(IKS_ERROR_CHAR_TO_X);
				case IKS_STRING: iksError(IKS_ERROR_STRING_TO_X);
			}break;
		
		case IKS_CHAR:
			switch(source){
				case IKS_CHAR: return IKS_CHAR;
				case IKS_INT: iksError(IKS_ERROR_WRONG_TYPE);
				case IKS_FLOAT: iksError(IKS_ERROR_WRONG_TYPE);
				case IKS_STRING: iksError(IKS_ERROR_STRING_TO_X);
				case IKS_BOOL: iksError(IKS_ERROR_WRONG_TYPE);
			}break;

		case IKS_STRING: 
			switch(source){
				case IKS_STRING: return IKS_STRING;
				case IKS_INT: iksError(IKS_ERROR_WRONG_TYPE);
				case IKS_FLOAT: iksError(IKS_ERROR_WRONG_TYPE);
				case IKS_CHAR: iksError(IKS_ERROR_CHAR_TO_X);
				case IKS_BOOL: iksError(IKS_ERROR_WRONG_TYPE);
			}break;
	}
}

void checkUsage(int option, int isA){
    //fprintf(stderr,"chkUsage\n");
	if (option == 0) //atribuicao
	{
		if (isA == FUNCAO)
			iksError(IKS_ERROR_FUNCTION);
		if (isA == VETOR)
			iksError(IKS_ERROR_VECTOR);
	}
}

int returnChecker(int func, int ret){
        switch(func){
		case IKS_INT:

			switch(ret){
				case IKS_INT: return IKS_INT;
				case IKS_FLOAT: return IKS_FLOAT;
				case IKS_BOOL: return IKS_INT;
				case IKS_CHAR: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_STRING: iksError(IKS_ERROR_WRONG_PAR_RETURN);
			}break;

		case IKS_FLOAT: 
			switch(ret){
				case IKS_INT: return IKS_FLOAT;
				case IKS_FLOAT: return IKS_FLOAT;
				case IKS_BOOL: return IKS_FLOAT;
				case IKS_CHAR: iksError(IKS_ERROR_CHAR_TO_X);
				case IKS_STRING: iksError(IKS_ERROR_STRING_TO_X);
			}break;

		case IKS_BOOL:
 		
			switch(ret){
				case IKS_INT: return IKS_INT;
				case IKS_FLOAT: return IKS_FLOAT;
				case IKS_BOOL: return IKS_BOOL;
				case IKS_CHAR: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_STRING: iksError(IKS_ERROR_WRONG_PAR_RETURN);
			}break;
		
		case IKS_CHAR:
			switch(ret){
				case IKS_CHAR: return IKS_CHAR;
				case IKS_INT: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_FLOAT: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_STRING: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_BOOL: iksError(IKS_ERROR_WRONG_PAR_RETURN);
			}break;

		case IKS_STRING: 
			switch(ret){
				case IKS_STRING: return IKS_STRING;
				case IKS_INT: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_FLOAT: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_CHAR: iksError(IKS_ERROR_WRONG_PAR_RETURN);
				case IKS_BOOL: iksError(IKS_ERROR_WRONG_PAR_RETURN);
			}break;
	}
}

void isNum(int param){
    if (param == IKS_CHAR)
        iksError(IKS_ERROR_CHAR_TO_X);
    if (param == IKS_STRING)
        iksError(IKS_ERROR_STRING_TO_X);
    if (param == IKS_BOOL)
        iksError(IKS_ERROR_WRONG_TYPE);
}

void isId(int param){
    if (param != AST_IDENTIFICADOR)
        iksError(IKS_ERROR_WRONG_PAR_INPUT);
}

void isOut(int param, int string){
    if ((param < AST_ARIM_SOMA || param > AST_ARIM_INVERSAO) && (param != AST_LITERAL || string != IKS_STRING))
        iksError(IKS_ERROR_WRONG_PAR_OUTPUT);
}

void checkArgs(intlist* func, intlist* args){
    if (func != NULL && args == NULL)
        iksError(IKS_ERROR_MISSING_ARGS);
    else if (func == NULL && args != NULL)
        iksError(IKS_ERROR_EXCESS_ARGS);
    else if (func == NULL && args == NULL)
        return;
    else if (func->val != args->val)
        iksError(IKS_ERROR_WRONG_TYPE_ARGS);
    else
        checkArgs(func->next, args->next);
}

