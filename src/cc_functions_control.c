#include "cc_functions_control.h"
#define DEBUG 0

UT_icd ast_icd = {sizeof(struct comp_tree_t), NULL, NULL, NULL};
UT_icd dict_item_icd = {sizeof(struct _comp_dict_item_t), NULL, NULL, NULL};
FunctionInfo functions_hash = NULL;
extern FunctionInfo current_func;

void push_function_param(struct _comp_dict_item_t *item) {
    if(!temporary_param_fifo) {
        utarray_new(temporary_param_fifo, &dict_item_icd);
    }
    
    utarray_push_back(temporary_param_fifo,item);
}

void push_function_local_var(struct _comp_dict_item_t  *item) {
    if(!temporary_local_var_fifo) {
        utarray_new(temporary_local_var_fifo, &dict_item_icd);
    }
    
    utarray_push_back(temporary_local_var_fifo,item);
}

FunctionInfo declare_function(ASTNode function_node) {
    if(!temporary_local_var_fifo) {
        utarray_new(temporary_local_var_fifo, &dict_item_icd);
    }
    
    if(!temporary_param_fifo) {
        utarray_new(temporary_param_fifo, &dict_item_icd);
    }
    
    int ra_pos = 0;
    int i;
    FunctionInfo fun = calloc(1,sizeof(struct function_info));
    fun->name = function_node->dict_item->text;
    fun->ast_node = function_node;
    
    //Variaveis locais
    
    utarray_new(fun->local_vars_names,&ut_str_icd);
    utarray_new(fun->local_vars_sizes,&ut_int_icd);
    utarray_new(fun->local_vars_pos,&ut_int_icd);
    
    fun->vars_pos = ra_pos;
    for(i=0;i<utarray_len(temporary_local_var_fifo);i++) {
        DictItem *var = (DictItem*) utarray_eltptr(temporary_local_var_fifo,i);
        if(!var->is_static) {
            utarray_push_back(fun->local_vars_names,&var->text);
            utarray_push_back(fun->local_vars_sizes,&var->size);
            int pos = ra_pos;
            utarray_push_back(fun->local_vars_pos,&pos);
            ra_pos = ra_pos + var->size;
        }
    }
    
    //Estado da Maquina (Registradores)
    //Guardar o que? Eu assumo que tudo esta o mais cedo possivel em memoria
    fun->state_pos = ra_pos;
    //Vinculo estatico
    fun->static_pos = ra_pos;
    ra_pos += 4;
    //Vinculo dinamico
    fun->din_pos = ra_pos;
    ra_pos += 4;
    //Valor retornado
    fun->return_value_pos = ra_pos;
    ra_pos += function_node->dict_item->size;
    
    //Parametros formais
    utarray_new(fun->param_names,&ut_str_icd);
    utarray_new(fun->param_sizes,&ut_int_icd);
    utarray_new(fun->param_pos,&ut_int_icd);
    fun->params_pos = ra_pos;
    for(i=0;i<utarray_len(temporary_param_fifo);i++) {
        DictItem *param = (DictItem*) utarray_eltptr(temporary_param_fifo,i);
        if(!param->is_static) {
            utarray_push_back(fun->param_names,&param->text);
            utarray_push_back(fun->param_sizes,&param->size);
            int pos = ra_pos;
            utarray_push_back(fun->param_pos,&pos);
            ra_pos = ra_pos + param->size;
        }
    }
    
    //Endereco de retorno
    fun->return_addr_pos = ra_pos;
    ra_pos += 4;
    fun->rarp_size = ra_pos;
    
    utarray_clear(temporary_local_var_fifo);
    utarray_clear(temporary_param_fifo);
    
    HASH_ADD_KEYPTR(hh, functions_hash, fun->name, strlen(fun->name), fun);
    
    //fprintf(stderr,"@@@@@@@Funcao %s declarada %p\n",fun->name,fun);
    return fun;
}

FunctionInfo find_function(char *name) {
    //fprintf(stderr,"@@@@@@@Procurando %s :: ",name);
    FunctionInfo fun = NULL;
    HASH_FIND_STR(functions_hash, name, fun);
    //fprintf(stderr,"@@@@@@@ %p\n",fun);
    return fun;
}

int find_param_pos(FunctionInfo f_info, DictItem *item) {
    int found = 0;
    int i = 0;
    char **p = NULL;
    if(DEBUG) fprintf(stderr,"@@@@Procurando por [%s] em [%s] {%d}\n",item->text,f_info->name,utarray_len(f_info->param_names));
    while (!found) {
        p = (char**)utarray_next(f_info->param_names,p);
        if(DEBUG) fprintf(stderr,"@@@@Percorrendo [%s]\n",*p);
        if(strcmp(*p,item->text)==0) found = 1;
        if(DEBUG) fprintf(stderr,"@@@@FOUND: [%d]\n",found);
        i++;
        if(DEBUG) fprintf(stderr,"@@@@POS: [%d]\n",i);
    }
    
    if(found) return *((int*) utarray_eltptr(f_info->param_pos,i-1));
    else return -999;
}

int find_var_pos(FunctionInfo f_info, DictItem *item) {
    int found = 0;
    int i = 0;
    char **p = NULL;
    if(DEBUG) fprintf(stderr,"@@@@Procurando por [%s] em [%s] {%d}\n",item->text,f_info->name,utarray_len(f_info->local_vars_names));
    while (!found) {
        p = (char**)utarray_next(f_info->local_vars_names,p);
        if(DEBUG) fprintf(stderr,"@@@@Percorrendo [%s]\n",*p);
        if(strcmp(*p,item->text)==0) found = 1;
        if(DEBUG) fprintf(stderr,"@@@@FOUND: [%d]\n",found);
        i++;
        if(DEBUG) fprintf(stderr,"@@@@POS: [%d]\n",i);
    }
    
    if(found) return *((int*) utarray_eltptr(f_info->local_vars_pos,i-1));
    else return -666;
}