#include "cc_misc.h"
#include "cc_dict.h"
#include "cc_tree.h"
#include "scopes.h"

extern int yylineno;
extern char *yytext;
extern DictItem *my_table;
extern ASTNode ast_root;

int getLineNumber (void)
{
  //implemente esta função
  return yylineno;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s na linha %d, no token %s\n", mensagem,yylineno,yytext); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
  //implemente esta função com rotinas de inicialização, se necessário
}

void main_finalize (void)
{
    //hash_print(my_table);
    destroyAllScopes();
    if(ast_root) ASTDestroy(ast_root);
    //hash_destroy(my_table);
    //fprintf(stdout,"\n::: Size of pointer %lu\n",sizeof(void*));
    
}
