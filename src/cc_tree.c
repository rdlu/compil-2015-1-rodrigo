#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cc_tree.h"
#include "cc_gv.h"
#include "main.h"
#include "cc_iloc.h"
#include "scopes.h"

ASTNode ASTNewNode(int nodeType, DictItem *dict_item, ASTNode filho_0, ASTNode filho_1, ASTNode filho_2, ASTNode filho_3 ){
    ASTNode newNode = calloc(1,sizeof(struct comp_tree_t));
    
    newNode->nodeType = nodeType;
    if(nodeType == AST_IDENTIFICADOR) {
        newNode->dict_item = sFindAllButCurrentDeclared(dict_item->text);
        if(!newNode->dict_item) {
            newNode->dict_item = dict_item;
        }
    } else {
        newNode->dict_item = dict_item;
    }
    if(newNode->dict_item) newNode->dict_item->used_by_ast++;
    newNode->tipoIKS = 0;
    newNode->size = 0;
    utstring_new(newNode->code);
    utstring_new(newNode->label_t);
    utstring_new(newNode->label_f);
    utstring_new(newNode->label);

	newNode->filho[0] = filho_0;
	newNode->filho[1] = filho_1;
	newNode->filho[2] = filho_2;
	//Filho 3 reservado para listas
    newNode->filho[3] = 0;
    if(filho_3 != 0) {
        fprintf(stderr,"Uso indevido do filho 3, ele foi reservado para uso de listas");
    }
    return newNode;
}

//Baseado em PREORDER
//funcao eh ponteiro para um funcao qualquer
//util para PRINT
void ASTPreorder(ASTNode a_node, void (*funcao)(ASTNode)) {
      //executa a funcao passada por ponteiro ANTES de percorrer (difere do DESTROY)
      funcao(a_node);
      //percorre em preorder
      int i = 0;
      for (i = 0; i < MAX_FILHOS; i++)
      {
        if(a_node->filho[i] != 0)
            if(a_node != 0) ASTPreorder(a_node->filho[i],funcao);
      }
}

//Baseado em POSTORDER
//funcao eh ponteiro para um funcao qualquer
//util para DELETE, DESTROY, FREE
void ASTPostorder(ASTNode a_node, void (*funcao)(ASTNode)) {
      //percorremos antes (e empilhamos recursivamente)
      int i = 0;
      for (i = 0; i < MAX_FILHOS; i++)
      {
        if(a_node->filho[i] != 0)
            ASTPostorder(a_node->filho[i],funcao);
      }
      //executamos a funcao apos
      funcao(a_node);
}

//Misto! Executa uma antes e uma depois do acesso aos filhos
//Util para graphviz
void ASTPreorderPostorder(ASTNode a_node, void (*funcao)(ASTNode), void (*funcao2)(ASTNode)) {
      funcao(a_node);
      //percorremos (e empilhamos recursivamente)
      int i = 0;
      for (i = 0; i < MAX_FILHOS+1; i++)
      {
        if(a_node->filho[i] != 0)
            ASTPostorder(a_node->filho[i],funcao);
      }
      //executamos a funcao2 apos
      funcao2(a_node);
}

void _ASTDelete(ASTNode a_node) {
    //fprintf(stdout,"AST %p ",a_node);
    if(a_node->dict_item) {
        a_node->dict_item->used_by_ast--;
        if(a_node->dict_item->used_by_ast == 0) dictItem_destroy(a_node->dict_item);
    }
    a_node->dict_item = NULL;
	free(a_node);
}

//TODO: Fazer o print para debug (graphviz eh melhor)
void _ASTPrint(ASTNode a_node) {
    //fprintf(output_file,"AST(%d,",a_node->nodeType);
    //fprintf(output_file,")\n");
}

void _ASTGraphvizDeclare(ASTNode a_node) {
    //void gv_declare (const int tipo, const void *pointer, const char *name);
    //fprintf(stderr,"AST %p %i",a_node,a_node->nodeType);
    switch (a_node->nodeType){
        case AST_FUNCAO:
        case AST_IDENTIFICADOR:
        case AST_LITERAL:
            switch(a_node->dict_item->token_type) {
                case SIMBOLO_LITERAL_CHAR:
                case SIMBOLO_LITERAL_STRING:
                    gv_declare(a_node->nodeType,a_node,a_node->dict_item->data.string_value);
                    break;
                default:
                    gv_declare(a_node->nodeType,a_node,a_node->dict_item->text);
            }
            
            break;
        default:
            gv_declare(a_node->nodeType,a_node,NULL);
      }
}

void _ASTGraphvizConnect(ASTNode a_node) {
    //void gv_connect (const void *p1, const void *p2);
    int i = 0;
    for (i = 0; i < MAX_FILHOS; i++) {
        if(a_node->filho[i] != 0)
            gv_connect(a_node,a_node->filho[i]);
    }
}

void ASTDestroy(ASTNode root_node) {
	ASTPostorder(root_node, _ASTDelete);
}

void ASTGraphviz(ASTNode a_node) {
//    ASTPostorder(a_node,_ASTGraphvizDeclare);
//    ASTPostorder(a_node,_ASTGraphvizConnect);
    
    ASTPreorder(a_node,_ASTGraphvizDeclare);
    ASTPreorder(a_node,_ASTGraphvizConnect);
    
    //ASTPreorderPostorder(a_node, _ASTGraphvizDeclare, _ASTGraphvizConnect);
}

void ASTPrint(ASTNode root_node) {
  ASTPreorder(root_node, _ASTPrint);
}
