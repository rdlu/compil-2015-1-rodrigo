/*
	Rodrigo Dlugokenski
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cc_dict.h"
#include "main.h"

extern int yylineno;
extern DictItem *current_table;

DictItem* dictItem_new(int token_type, char* raw_data) {
	DictItem* item = calloc(1,sizeof(DictItem));
    item->text = strdup(raw_data);
    utstring_new(item->mem_pos);
	item->linenum = getLineNumber();
	item->token_type = token_type;
    item->identifier_type = 0;
    item->used_by_ast = 0;
    item->scope_level = 0;
    utarray_new(item->dimension_size,&ut_int_icd);

	switch(token_type) {
		case SIMBOLO_LITERAL_INT:
			item->data.int_value = atoi(raw_data);
            item->size = atoi(raw_data);
			break;
		case SIMBOLO_LITERAL_FLOAT:
			item->data.float_value = atof(raw_data);
			break;
		case SIMBOLO_LITERAL_STRING:
			item->data.string_value = strndup(raw_data+1,strlen(raw_data)-2);
			break;
		case SIMBOLO_LITERAL_CHAR:
			item->data.string_value = strndup(raw_data+1,1);
			break;
	}
	return item;
}

void dictItem_destroy(DictItem *item) {
    if(item->used_by_ast == 0) {
        //fprintf(stderr,"destroy %s %p\n",item->text,item);
        if(item->token_type == SIMBOLO_LITERAL_STRING || item->token_type == SIMBOLO_LITERAL_CHAR)
            free(item->data.string_value);
        utstring_free(item->mem_pos);
        utarray_free(item->dimension_size);
        free(item->text);
	    free(item);
    }
	
}

DictItem* hash_new() {
    DictItem *dict_item = NULL;    /* important! initialize to NULL */
    dict_item = hash_add(dict_item,"FAAAAKE_HEADER!",6666);
    return dict_item;
}

DictItem* hash_find(DictItem *hashtable, char *text) {
    DictItem *item;
    //fprintf(stderr,"find called :: [%s] :: [%p]\n",text,hashtable);
    HASH_FIND_STR(hashtable, text, item);
    //if(item) fprintf(stderr,"find result :: [%s] :: [%p]\n",item->text,hashtable);
    return item;
}

//remove do hash
DictItem* hash_remove(DictItem *hashtable, char *text) {
    DictItem *item = hash_find(hashtable,text);
    HASH_DEL(hashtable,item);
    return item;
}

DictItem* hash_add(DictItem *hashtable, char *text, int type)
{
    //fprintf(stderr,"item Called :: [%s] :: [%p] CUR [%p]\n",text,hashtable,current_table);
    DictItem *item = hash_find(hashtable,text);

    //Ja existe
    if (item) {
        //fprintf(stderr,"item found :: [%s] :: [%p]\n",item->text,hashtable);
    	return item;
    } else {
        item = dictItem_new(type,text);
    	//printf("Alocando novo nodo \n");
        HASH_ADD_KEYPTR( hh, hashtable, item->text, strlen(item->text), item);
        //fprintf(stderr,"item added :: [%s] :: [%p] CUR [%p]\n",item->text,hashtable,current_table);
        return item;
    }
}


void hash_destroy(DictItem *hashtable) {
  DictItem *current_item, *tmp;

  HASH_ITER(hh, hashtable, current_item, tmp) {
    HASH_DEL(hashtable,current_item);
    dictItem_destroy(current_item);
  }
}


