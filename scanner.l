/*
	Grupo Heroes of The Storm
	Arthur Ribeiro
	Rodrigo Dlugokenski
	Rafael de Jesus Martins
*/

digit [0-9]
letter [A-Za-z_]

%{
#include <stdlib.h>
#include <string.h>
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "main.h"

DictItem *current_table = NULL;

%}

%x comment
%x linecomment

%option yylineno

%%

"int"    { return TK_PR_INT;    }/*Palavras Reservadas do sistema*/
"float"  { return TK_PR_FLOAT;  }
"bool"   { return TK_PR_BOOL;   }
"char"   { return TK_PR_CHAR;   }
"string" { return TK_PR_STRING; }

"if"     { return TK_PR_IF;     }
"then"   { return TK_PR_THEN;   }
"else"   { return TK_PR_ELSE;   }
"while"  { return TK_PR_WHILE;  }
"do"     { return TK_PR_DO;     }

"input"  { return TK_PR_INPUT;  }
"output" { return TK_PR_OUTPUT; }

"return" { return TK_PR_RETURN; }

"const"  { return TK_PR_CONST;  }
"static" { return TK_PR_STATIC; }


"<="     { return TK_OC_LE;     }/*Operadores de Compostos*/
">="     { return TK_OC_GE;     }
"=="     { return TK_OC_EQ;     }
"!="     { return TK_OC_NE;     }
"&&"     { return TK_OC_AND;    }
"||"     { return TK_OC_OR;     }



[,;:()\[\]{}+\-*\/<>=!&$%] { return yytext[0]; } /* Caracteres especiais */



"false" { yylval.dict_item = hash_add(current_table, yytext, SIMBOLO_LITERAL_BOOL); return TK_LIT_FALSE; }/*Literais*/
"true"  { yylval.dict_item = hash_add(current_table, yytext, SIMBOLO_LITERAL_BOOL); return TK_LIT_TRUE;  }

{digit}+	{ yylval.dict_item = hash_add(current_table, yytext, SIMBOLO_LITERAL_INT); return TK_LIT_INT; }


{digit}+"."{digit}+	{ yylval.dict_item = hash_add(current_table, yytext, SIMBOLO_LITERAL_FLOAT); return TK_LIT_FLOAT; }



({letter})*({letter}|{digit})*  { yylval.dict_item = hash_add(current_table, yytext, SIMBOLO_IDENTIFICADOR); return TK_IDENTIFICADOR; }


\"([^\\\"]|\\.)*\"  { yylval.dict_item = hash_add(current_table,yytext, SIMBOLO_LITERAL_STRING);
	return TK_LIT_STRING; }

\'([^\\\']|\\.)*\'  { yylval.dict_item = hash_add(current_table,yytext, SIMBOLO_LITERAL_CHAR);
	 return TK_LIT_CHAR; }



"//"				{ BEGIN linecomment; }/*Ingnorar comentarios*/
<linecomment>.
<linecomment><<EOF>>	{  }
<linecomment>"\n"		{ BEGIN INITIAL;  }

"/*"				{ BEGIN comment; }
<comment>"\n"		{ }
<comment><<EOF>>		{ yyerror("Premature end of file inside block comment");  }
<comment>.
<comment>"*/"		{ BEGIN INITIAL; }


[  \t\n]+ { }
.					{ return TOKEN_ERRO; }

%%
